package cz.cvut.fit.tjv.performance_reservation.api.views;

public class PerformanceViews {
    public static class ViewWithoutId {
    }

    public static class ViewWithId extends ViewWithoutId {
    }
}
