package cz.cvut.fit.tjv.performance_reservation.api.dto;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonView;
import cz.cvut.fit.tjv.performance_reservation.api.views.PerformanceViews;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;

@Getter
@Setter
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor

public class PerformanceDto {
    @Schema(requiredMode = Schema.RequiredMode.REQUIRED)
    @JsonView(PerformanceViews.ViewWithId.class)
    private Long id;
    @Schema(requiredMode = Schema.RequiredMode.REQUIRED, type = "string", example = "24/05/2015 19:30")
    @JsonView(PerformanceViews.ViewWithoutId.class)
    @JsonFormat(pattern = "dd/MM/yyyy HH:mm")
    private LocalDateTime dateTimeFrom;
    @Schema(requiredMode = Schema.RequiredMode.REQUIRED, type = "string", example = "24/05/2015 23:30")
    @JsonView(PerformanceViews.ViewWithoutId.class)
    @JsonFormat(pattern = "dd/MM/yyyy HH:mm")
    private LocalDateTime dateTimeTo;
}
