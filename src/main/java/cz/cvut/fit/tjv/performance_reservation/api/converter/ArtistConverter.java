package cz.cvut.fit.tjv.performance_reservation.api.converter;

import cz.cvut.fit.tjv.performance_reservation.api.dto.ArtistDto;
import cz.cvut.fit.tjv.performance_reservation.domain.Artist;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class ArtistConverter {

    public static Artist fromDto(ArtistDto dto) {
        return new Artist(dto.getId(), dto.getName(), dto.getGenre());
    }

    public static ArtistDto toDto(Artist artist) {
        return new ArtistDto(artist.getId(), artist.getName(), artist.getGenre());
    }

    public static Collection<ArtistDto> toDtoMany(Collection<Artist> artists) {
        List<ArtistDto> artistDtos = new ArrayList<>();
        artists.forEach(artist -> artistDtos.add(toDto(artist)));
        return artistDtos;
//        return artists.stream()
//                .map(ArtistConverter::toDto)
//                .collect(Collectors.toList());
    }
}
