package cz.cvut.fit.tjv.performance_reservation.business.exceptions;

public class PerformanceTimeIntersectionException extends RuntimeException {
    public PerformanceTimeIntersectionException() {super("Artist's performance time is intersecting with the one they applies to");}
}
