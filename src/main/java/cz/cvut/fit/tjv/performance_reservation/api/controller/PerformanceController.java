package cz.cvut.fit.tjv.performance_reservation.api.controller;

import com.fasterxml.jackson.annotation.JsonView;
import cz.cvut.fit.tjv.performance_reservation.api.converter.ArtistConverter;
import cz.cvut.fit.tjv.performance_reservation.api.converter.EstablishmentConverter;
import cz.cvut.fit.tjv.performance_reservation.api.dto.ArtistDto;
import cz.cvut.fit.tjv.performance_reservation.api.dto.EstablishmentDto;
import cz.cvut.fit.tjv.performance_reservation.api.dto.PerformanceDto;
import cz.cvut.fit.tjv.performance_reservation.api.exception.NullIdException;
import cz.cvut.fit.tjv.performance_reservation.api.views.ArtistViews;
import cz.cvut.fit.tjv.performance_reservation.api.views.EstablishmentViews;
import cz.cvut.fit.tjv.performance_reservation.api.views.PerformanceViews;
import cz.cvut.fit.tjv.performance_reservation.business.exceptions.NonExistingEntityException;
import cz.cvut.fit.tjv.performance_reservation.business.PerformanceService;
import cz.cvut.fit.tjv.performance_reservation.domain.Performance;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import java.util.Collection;

import static cz.cvut.fit.tjv.performance_reservation.api.converter.PerformanceConverter.fromDto;
import static cz.cvut.fit.tjv.performance_reservation.api.converter.PerformanceConverter.toDto;
import static cz.cvut.fit.tjv.performance_reservation.api.converter.PerformanceConverter.*;
import static org.springframework.http.HttpStatus.NO_CONTENT;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.http.MediaType.TEXT_PLAIN_VALUE;

@RestController
public class PerformanceController {
    private final PerformanceService performanceService;

    public PerformanceController(PerformanceService performanceService) {
        this.performanceService = performanceService;
    }

    @Operation(summary = "Create performance")
    @ApiResponse(content = @Content(mediaType = APPLICATION_JSON_VALUE,
            schema = @Schema(implementation = PerformanceDto.class)),
            responseCode = "200")
    @JsonView(PerformanceViews.ViewWithId.class)
    @PostMapping("/performances")
    public PerformanceDto create(@JsonView(PerformanceViews.ViewWithoutId.class)
                                 @RequestBody PerformanceDto performanceDto) {
        Performance performance = performanceService.create(fromDto(performanceDto));
        if (performance.getId() == null) {
            throw new NullIdException();
        }
        return toDto(performance);
    }

    @Operation(summary = "Read performance by ID")
    @ApiResponse(content = @Content(mediaType = APPLICATION_JSON_VALUE,
                schema = @Schema(implementation = PerformanceDto.class)),
                responseCode = "200")
    @ApiResponse(responseCode = "404",
                description =  "Performance not found",
                content = @Content(mediaType = TEXT_PLAIN_VALUE))
    @JsonView(PerformanceViews.ViewWithId.class)
    @GetMapping("/performances/{id}")
    public PerformanceDto readById(@PathVariable Long id) {
        return toDto(performanceService.readById(id).orElseThrow(EntityNotFoundException::new));
    }

    @Operation(summary = "Read all performances")
    @ApiResponse(content = @Content(mediaType = APPLICATION_JSON_VALUE,
                array = @ArraySchema(schema = @Schema(implementation = PerformanceDto.class))),
                responseCode = "200")
    @JsonView(PerformanceViews.ViewWithId.class)
    @GetMapping("/performances")
    public Collection<PerformanceDto> readAll() {
        return toDtoMany(performanceService.readAll());
    }

    @Operation(summary = "Read establishments to performance")
    @ApiResponse(content = @Content(mediaType = APPLICATION_JSON_VALUE,
            array = @ArraySchema(schema = @Schema(implementation = EstablishmentDto.class))),
            responseCode = "200")
    @ApiResponse(responseCode = "404",
            content = @Content(mediaType = TEXT_PLAIN_VALUE))
    @JsonView(EstablishmentViews.ViewWithId.class)
    @GetMapping("/performances/{id}/establishments")
    public EstablishmentDto readEstablishmentToPerformance(@PathVariable Long id) {
        Performance performance = performanceService.readById(id).orElseThrow(NonExistingEntityException::new);
        if (performance.getEstablishment() == null) {
            return null;
        }
        return EstablishmentConverter.toDto(performance.getEstablishment());
    }

    @Operation(summary = "Read artists of performance")
    @ApiResponse(content = @Content(mediaType = APPLICATION_JSON_VALUE,
            array = @ArraySchema(schema = @Schema(implementation = ArtistDto.class))),
            responseCode = "200")
    @ApiResponse(responseCode = "404",
            content = @Content(mediaType = TEXT_PLAIN_VALUE))
    @JsonView(ArtistViews.ViewWithId.class)
    @GetMapping("/performances/{perfId}/artists")
    public Collection<ArtistDto> readArtistsOfPerformance(@PathVariable Long perfId) {
        return ArtistConverter.toDtoMany(performanceService.readArtistsOfPerformance(perfId));
    }

    @Operation(summary = "Update performance")
    @ApiResponse(content = @Content(mediaType = APPLICATION_JSON_VALUE,
                schema = @Schema(implementation = PerformanceDto.class)),
                responseCode = "200")
    @ApiResponse(responseCode = "404",
                content = @Content(mediaType = TEXT_PLAIN_VALUE))
    @ApiResponse(responseCode = "422",
                content = @Content(mediaType = TEXT_PLAIN_VALUE))
    @ApiResponse(responseCode = "409",
                content = @Content(mediaType = TEXT_PLAIN_VALUE))
    @JsonView(PerformanceViews.ViewWithId.class)
    @PutMapping("/performances/{id}")
    public PerformanceDto update(@PathVariable Long id,
                                 @JsonView(PerformanceViews.ViewWithoutId.class)
                                 @RequestBody PerformanceDto performanceDto) {
        Performance performance = fromDto(performanceDto);
        performance.setId(id);
        return toDto(performanceService.update(performance));
    }

    @Operation(summary = "Update artist of performance")
    @ApiResponse(responseCode = "204")
    @ApiResponse(responseCode = "404",
            content = @Content(mediaType = TEXT_PLAIN_VALUE))
    @ApiResponse(responseCode = "409",
            content = @Content(mediaType = TEXT_PLAIN_VALUE))
    @ResponseStatus(NO_CONTENT)
    @PutMapping("/performances/{perfId}/artists/{artId}")
    public void updateArtistOfPerformance(@PathVariable Long perfId, @PathVariable Long artId) {
        performanceService.updateArtistOfPerformance(perfId, artId);
    }

    @Operation(summary = "Delete performance by ID")
    @DeleteMapping("/performances/{id}")
    @ResponseStatus(NO_CONTENT)
    public void deleteById(@PathVariable Long id) {
        performanceService.deleteById(id);
    }

    @Operation(summary = "Delete establishment to performance")
    @ApiResponse(responseCode = "404",
            content = @Content(mediaType = TEXT_PLAIN_VALUE))
    @ApiResponse(responseCode = "204")
    @DeleteMapping("/performances/{id}/establishments")
    @ResponseStatus(NO_CONTENT)
    public void deleteEstablishmentToPerformance(@PathVariable Long id) {
        performanceService.deleteEstablishmentToPerformance(id);
    }

    @Operation(summary = "Delete artist of performance")
    @ApiResponse(responseCode = "404",
            content = @Content(mediaType = TEXT_PLAIN_VALUE))
    @ApiResponse(responseCode = "204")
    @ResponseStatus(NO_CONTENT)
    @DeleteMapping("/performances/{perfId}/artists/{artId}")
    public void deleteArtistOfPerformance(@PathVariable Long perfId, @PathVariable Long artId) {
        performanceService.deleteArtistOfPerformance(perfId, artId);
    }
}


