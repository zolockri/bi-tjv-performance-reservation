package cz.cvut.fit.tjv.performance_reservation.business;

import cz.cvut.fit.tjv.performance_reservation.business.exceptions.*;
import cz.cvut.fit.tjv.performance_reservation.dao.PerformanceJpaRepository;
import cz.cvut.fit.tjv.performance_reservation.domain.Artist;
import cz.cvut.fit.tjv.performance_reservation.domain.Performance;
import lombok.NonNull;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Collection;
import java.util.Set;

@Service
public class PerformanceService extends AbstractCrudService<Long, Performance, PerformanceJpaRepository> {
    private final ArtistService artistService;

    public PerformanceService(PerformanceJpaRepository repository, ArtistService artistService) {
        super(repository);
        this.artistService = artistService;
    }

    @Transactional
    public void deleteEstablishmentToPerformance(@NonNull Long perfId) {
        Performance performance = readById(perfId).orElseThrow(NonExistingEntityException::new);
        performance.setEstablishment(null);
    }

    @Transactional
    public Collection<Artist> readArtistsOfPerformance(@NonNull Long perfId) {
        Performance performance = readById(perfId).orElseThrow(NonExistingEntityException::new);
        return performance.getPerformingArtists();
    }

    @Transactional
    public void updateArtistOfPerformance(@NonNull Long perfId, @NonNull Long artId) {
        Performance performance = readById(perfId).orElseThrow(NonExistingEntityException::new);
        Artist artist = artistService.readById(artId).orElseThrow(NonExistingEntityException::new);
        if (!artist.getArtistsPerformances().contains(performance)) {
            checkArtistsPerformancesNotIntersecting(performance, artist);
        }
        performance.getPerformingArtists().add(artist);
    }

    private void checkArtistsPerformancesNotIntersecting(Performance performance, Artist artist) {
        Set<Performance> artistsPerformances = artist.getArtistsPerformances();
        artistsPerformances.forEach(artistsPerformance -> {
            if (!artistsPerformance.getId().equals(performance.getId())
                    && performance.getDateTimeTo().isAfter(artistsPerformance.getDateTimeFrom())
                    && artistsPerformance.getDateTimeTo().isAfter(performance.getDateTimeFrom())) {
                throw new PerformanceTimeIntersectionException();
            }
        });
    }

    @Transactional
    public void deleteArtistOfPerformance(@NonNull Long perfId, @NonNull Long artId) {
        Performance performance = readById(perfId).orElseThrow(NonExistingEntityException::new);
        Artist artist = artistService.readById(artId).orElseThrow(NonExistingEntityException::new);
        performance.getPerformingArtists().remove(artist);
    }

    @Override
    protected void updateRelations(Performance entity, Performance existingEntity) {
        entity.setEstablishment(existingEntity.getEstablishment());
    }

    @Override
    protected void checkNonNullableFields(Performance performance) {
        if (performance.getDateTimeFrom() == null || performance.getDateTimeTo() == null) {
            throw new NullEntityFieldException();
        }
    }

    @Override
    protected void checkRelationIntegrityConstraints(Performance entity, Performance existingEntity) {
        existingEntity.getPerformingArtists().forEach(artist -> checkArtistsPerformancesNotIntersecting(entity, artist));
    }

    @Override
    protected void checkTimeOrder(Performance performance) {
        if (performance.getDateTimeFrom().compareTo(performance.getDateTimeTo()) >= 0) {
            throw new DateFromBiggerThanDateTo();
        }
    }
}
