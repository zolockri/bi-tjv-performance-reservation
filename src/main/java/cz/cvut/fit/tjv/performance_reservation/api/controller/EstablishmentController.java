package cz.cvut.fit.tjv.performance_reservation.api.controller;


import com.fasterxml.jackson.annotation.JsonView;
import cz.cvut.fit.tjv.performance_reservation.api.converter.EstablishmentConverter;
import cz.cvut.fit.tjv.performance_reservation.api.converter.PerformanceConverter;
import cz.cvut.fit.tjv.performance_reservation.api.dto.ArtistDto;
import cz.cvut.fit.tjv.performance_reservation.api.dto.EstablishmentDto;
import cz.cvut.fit.tjv.performance_reservation.api.dto.PerformanceDto;
import cz.cvut.fit.tjv.performance_reservation.api.exception.NullIdException;
import cz.cvut.fit.tjv.performance_reservation.api.views.EstablishmentViews;
import cz.cvut.fit.tjv.performance_reservation.api.views.PerformanceViews;
import cz.cvut.fit.tjv.performance_reservation.business.EstablishmentService;
import cz.cvut.fit.tjv.performance_reservation.business.exceptions.NonExistingEntityException;
import cz.cvut.fit.tjv.performance_reservation.domain.Establishment;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import java.util.Collection;

import static cz.cvut.fit.tjv.performance_reservation.api.converter.EstablishmentConverter.fromDto;
import static cz.cvut.fit.tjv.performance_reservation.api.converter.EstablishmentConverter.toDtoMany;
import static cz.cvut.fit.tjv.performance_reservation.api.converter.EstablishmentConverter.toDto;
import static org.springframework.http.HttpStatus.NO_CONTENT;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.http.MediaType.TEXT_PLAIN_VALUE;

@RestController
public class EstablishmentController {
    private final EstablishmentService establishmentService;

    public EstablishmentController(EstablishmentService establishmentService) {
        this.establishmentService = establishmentService;
    }

    @Operation(summary = "Create establishment")
    @ApiResponse(content = @Content(mediaType = APPLICATION_JSON_VALUE,
            schema = @Schema(implementation = EstablishmentDto.class)),
            responseCode = "200")
    @JsonView(EstablishmentViews.ViewWithId.class)
    @PostMapping("/establishments")
    public EstablishmentDto create(
            @JsonView(EstablishmentViews.ViewWithoutId.class)
            @RequestBody EstablishmentDto establishmentDto) {
        Establishment establishment = establishmentService.create(fromDto(establishmentDto));
        if (establishment.getId() == null) {
            throw new NullIdException();
        }
        return EstablishmentConverter.toDto(establishment);
    }

    @Operation(summary = "Read establishment by ID")
    @ApiResponse(content = @Content(mediaType = APPLICATION_JSON_VALUE,
            schema = @Schema(implementation = EstablishmentDto.class)),
            responseCode = "200")
    @ApiResponse(responseCode = "404",
            description =  "Establishment not found",
            content = @Content(mediaType = TEXT_PLAIN_VALUE))
    @JsonView(EstablishmentViews.ViewWithId.class)
    @GetMapping("/establishments/{id}")
    public EstablishmentDto readById(@PathVariable Long id) {
        return toDto(establishmentService.readById(id).orElseThrow(EntityNotFoundException::new));
    }

    @Operation(summary = "Read all establishments")
    @ApiResponse(content = @Content(mediaType = APPLICATION_JSON_VALUE,
            array = @ArraySchema(schema = @Schema(implementation = EstablishmentDto.class))),
            responseCode = "200")
    @JsonView(EstablishmentViews.ViewWithId.class)
    @GetMapping("/establishments")
    public Collection<EstablishmentDto> readAll() {
        return toDtoMany(establishmentService.readAll());
    }

    @Operation(summary = "Read performances in establishment")
    @ApiResponse(content = @Content(mediaType = APPLICATION_JSON_VALUE,
            array = @ArraySchema(schema = @Schema(implementation = PerformanceDto.class))),
            responseCode = "200")
    @ApiResponse(responseCode = "404",
            content = @Content(mediaType = TEXT_PLAIN_VALUE))
    @JsonView(PerformanceViews.ViewWithId.class)
    @GetMapping("/establishments/{estId}/performances")
    public Collection<PerformanceDto> readPerformancesInEstablishment(@PathVariable Long estId) {
        return PerformanceConverter.toDtoMany(establishmentService.readPerformancesInEstablishment(estId));
    }

    @Operation(summary = "Update establishment")
    @ApiResponse(content = @Content(mediaType = APPLICATION_JSON_VALUE,
            schema = @Schema(implementation = EstablishmentDto.class)),
            responseCode = "200")
    @ApiResponse(responseCode = "404",
            content = @Content(mediaType = TEXT_PLAIN_VALUE))
    @ApiResponse(responseCode = "422",
            content = @Content(mediaType = TEXT_PLAIN_VALUE))
    @JsonView(EstablishmentViews.ViewWithId.class)
    @PutMapping("/establishments/{id}")
    public EstablishmentDto update(@PathVariable Long id,
                                   @JsonView(EstablishmentViews.ViewWithoutId.class)
                                   @RequestBody EstablishmentDto establishmentDto) {
        Establishment establishment = fromDto(establishmentDto);
        establishment.setId(id);
        return toDto(establishmentService.update(establishment));
    }

    @Operation(summary = "Delete establishment by ID")
    @DeleteMapping("/establishments/{id}")
    @ResponseStatus(NO_CONTENT)
    public void deleteById(@PathVariable Long id) {
        establishmentService.deleteById(id);
    }

    @Operation(summary = "Update performance to establishment")
    @ApiResponse(responseCode = "204")
    @ApiResponse(responseCode = "404",
            content = @Content(mediaType = TEXT_PLAIN_VALUE))
    @PutMapping("/establishments/{establishmentId}/performances/{performanceId}")
    @ResponseStatus(NO_CONTENT)
    public void updatePerformanceToEstablishment(@PathVariable Long establishmentId, @PathVariable Long performanceId) {
        establishmentService.updatePerformanceToEstablishment(performanceId, establishmentId);
    }
}
