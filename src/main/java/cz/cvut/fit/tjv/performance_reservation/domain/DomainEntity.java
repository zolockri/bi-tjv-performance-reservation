package cz.cvut.fit.tjv.performance_reservation.domain;

import java.io.Serializable;

public interface DomainEntity<ID> extends Serializable {
    ID getId();
}
