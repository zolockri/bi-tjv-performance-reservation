package cz.cvut.fit.tjv.performance_reservation.business.exceptions;

/**
 * A checked exception indicating problem related to existence of an entity.
 */
public class NonExistingEntityException extends RuntimeException {
    public NonExistingEntityException() {
    }

    public <E> NonExistingEntityException(E entity) {
        super("Illegal state of entity " + entity);
    }

    public NonExistingEntityException(String s) {
        super(s);
    }
}

