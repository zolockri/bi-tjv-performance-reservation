package cz.cvut.fit.tjv.performance_reservation.business;

import cz.cvut.fit.tjv.performance_reservation.business.exceptions.DeleteNonExistingEntityException;
import cz.cvut.fit.tjv.performance_reservation.business.exceptions.NonExistingEntityException;
import cz.cvut.fit.tjv.performance_reservation.domain.DomainEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.transaction.Transactional;
import java.util.Collection;
import java.util.Optional;

/**
 * Common superclass for business logic of all entities supporting operations Create, Read, Update, Delete.
 * @param <K> Type of (primary) key.
 * @param <E> Type of entity
 */
public abstract class AbstractCrudService<K, E extends DomainEntity<K>, R extends JpaRepository<E, K>> {
    /**
     * Reference to data (persistence) layer.
     */
    protected final R repository;

    protected AbstractCrudService(R repository) {
        this.repository = repository;
    }

    /**
     * Attempts to create a new entity
     * @param entity entity to be stored
     */

    @Transactional
    public E create(E entity) {
        checkNonNullableFields(entity);
        checkTimeOrder(entity);
        return repository.save(entity);
    }
    protected void checkRelationIntegrityConstraints(E entity, E existingEntity) {}

    protected void checkTimeOrder(E entity) {}

    protected void checkNonNullableFields(E entity) {}

    @Transactional
    public Optional<E> readById(K id) {
        return repository.findById(id);
    }

    @Transactional
    public Collection<E> readAll() {
        return repository.findAll();
    }

    /** Attempts to replace an already stored entity.
     * @param entity the new state of the entity to be updated
     */
    @Transactional
    public E update(E entity) {
        E existingEntity = readById(entity.getId()).orElseThrow(NonExistingEntityException::new);
        checkNonNullableFields(entity);
        checkTimeOrder(entity);
        checkRelationIntegrityConstraints(entity, existingEntity);
        updateRelations(entity, existingEntity);
        return repository.save(entity);
    }

    @Transactional
    public void deleteById(K id) {
        if (!repository.existsById(id)) {
            throw new DeleteNonExistingEntityException();
        }
        deleteRelationsById(id);
        repository.deleteById(id);
    }

    protected void deleteRelationsById(K id) {}

    protected void updateRelations(E entity, E existingEntity) {}

}