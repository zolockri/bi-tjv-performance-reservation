package cz.cvut.fit.tjv.performance_reservation.api.exception;

import cz.cvut.fit.tjv.performance_reservation.business.exceptions.*;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.persistence.EntityNotFoundException;

@ControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {
    @ExceptionHandler(NonExistingEntityException.class)
    protected ResponseEntity<Object> handleEntityState(Exception e, WebRequest r) {
        String responseBody = "Entity not found";
        return handleExceptionInternal(e, responseBody, new HttpHeaders(), HttpStatus.NOT_FOUND, r);
    }
    @ExceptionHandler(EntityNotFoundException.class)
    protected ResponseEntity<Object> handleEntityNotFound(Exception e, WebRequest r) {
        String responseBody = "Entity not found";
        return handleExceptionInternal(e, responseBody, new HttpHeaders(), HttpStatus.NOT_FOUND, r);
    }

    @ExceptionHandler(NullIdException.class)
    protected ResponseEntity<Object> handleEntityNullId(Exception e, WebRequest r) {
        String responseBody = "Entity ID not found";
        return handleExceptionInternal(e, responseBody, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR, r);
    }

    @ExceptionHandler(DeleteNonExistingEntityException.class)
    protected ResponseEntity<Object> handleDeleteNonExistingEntity(Exception e, WebRequest r) {
        return handleExceptionInternal(e, null, new HttpHeaders(), HttpStatus.NO_CONTENT, r);
    }

    @ExceptionHandler(NullEntityFieldException.class)
    protected ResponseEntity<Object> handleEntityNullField(Exception e, WebRequest r) {
        String responseBody = "Entity null field";
        return handleExceptionInternal(e, responseBody, new HttpHeaders(), HttpStatus.UNPROCESSABLE_ENTITY, r);
    }

    @ExceptionHandler(DateFromBiggerThanDateTo.class)
    protected ResponseEntity<Object> handleDateFromBiggerThanDateTo(Exception e, WebRequest r) {
        String responseBody = "Wrong date-time line: start time must be before end time";
        return handleExceptionInternal(e, responseBody, new HttpHeaders(), HttpStatus.UNPROCESSABLE_ENTITY, r);
    }

    @ExceptionHandler(PerformanceTimeIntersectionException.class)
    protected ResponseEntity<Object> handlePerformanceTimeIntersectionException(Exception e, WebRequest r) {
        String responseBody = "Artist's performances intersecting";
        return handleExceptionInternal(e, responseBody, new HttpHeaders(), HttpStatus.CONFLICT, r);
    }
}
