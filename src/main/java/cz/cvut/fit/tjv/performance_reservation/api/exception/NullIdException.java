package cz.cvut.fit.tjv.performance_reservation.api.exception;

public class NullIdException extends RuntimeException {
    public NullIdException() {
    }

    public <E> NullIdException(E entity) {
        super("Entity null id " + entity);
    }
}
