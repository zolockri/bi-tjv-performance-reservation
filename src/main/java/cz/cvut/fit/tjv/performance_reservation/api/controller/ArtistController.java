package cz.cvut.fit.tjv.performance_reservation.api.controller;

import com.fasterxml.jackson.annotation.JsonView;
import cz.cvut.fit.tjv.performance_reservation.api.converter.PerformanceConverter;
import cz.cvut.fit.tjv.performance_reservation.api.dto.ArtistDto;
import cz.cvut.fit.tjv.performance_reservation.api.dto.EstablishmentDto;
import cz.cvut.fit.tjv.performance_reservation.api.dto.PerformanceDto;
import cz.cvut.fit.tjv.performance_reservation.api.exception.NullIdException;
import cz.cvut.fit.tjv.performance_reservation.api.views.ArtistViews;
import cz.cvut.fit.tjv.performance_reservation.api.views.EstablishmentViews;
import cz.cvut.fit.tjv.performance_reservation.api.views.PerformanceViews;
import cz.cvut.fit.tjv.performance_reservation.business.ArtistService;
import cz.cvut.fit.tjv.performance_reservation.domain.Artist;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import java.util.Collection;

import static cz.cvut.fit.tjv.performance_reservation.api.converter.ArtistConverter.*;
import static org.springframework.http.HttpStatus.NO_CONTENT;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.http.MediaType.TEXT_PLAIN_VALUE;

@RestController
public class ArtistController {

    private final ArtistService artistService;

    public ArtistController(ArtistService artistService) {
        this.artistService = artistService;
    }

    @Operation(summary = "Create artist")
    @ApiResponse(content = @Content(mediaType = APPLICATION_JSON_VALUE,
            schema = @Schema(implementation = ArtistDto.class)),
            responseCode = "200")
    @JsonView(ArtistViews.ViewWithId.class)
    @PostMapping("/artists")
    public ArtistDto create(@JsonView(ArtistViews.ViewWithoutId.class) @RequestBody ArtistDto artistDto) {
        Artist artist = artistService.create(fromDto(artistDto));
        if (artist.getId() == null) {
            throw new NullIdException();
        }
        return toDto(artist);
    }

    @Operation(summary = "Read artist by ID")
    @ApiResponse(content = @Content(mediaType = APPLICATION_JSON_VALUE,
            schema = @Schema(implementation = PerformanceDto.class)),
            responseCode = "200")
    @ApiResponse(responseCode = "404",
            description =  "Artist not found",
            content = @Content(mediaType = TEXT_PLAIN_VALUE))
    @JsonView(ArtistViews.ViewWithId.class)
    @GetMapping("/artists/{id}")
    public ArtistDto readById(@PathVariable Long id){
        return toDto(artistService.readById(id).orElseThrow(EntityNotFoundException::new));
    }

    @Operation(summary = "Read all artist")
    @ApiResponse(content = @Content(mediaType = APPLICATION_JSON_VALUE,
            array = @ArraySchema(schema = @Schema(implementation = ArtistDto.class))),
            responseCode = "200")
    @JsonView(ArtistViews.ViewWithId.class)
    @GetMapping("/artists")
    public Collection<ArtistDto> readAll(){
        return toDtoMany(artistService.readAll());
    }

    @Operation(summary = "Read performances of artist")
    @ApiResponse(content = @Content(mediaType = APPLICATION_JSON_VALUE,
            array = @ArraySchema(schema = @Schema(implementation = PerformanceDto.class))),
            responseCode = "200")
    @ApiResponse(responseCode = "404",
            content = @Content(mediaType = TEXT_PLAIN_VALUE))
    @JsonView(PerformanceViews.ViewWithId.class)
    @GetMapping("/artists/{artId}/performances")
    public Collection<PerformanceDto> readPerformancesOfArtist(@PathVariable Long artId) {
        return PerformanceConverter.toDtoMany(artistService.readPerformancesOfArtist(artId));
    }

    @Operation(summary = "Update artist")
    @ApiResponse(content = @Content(mediaType = APPLICATION_JSON_VALUE,
            schema = @Schema(implementation = ArtistDto.class)),
            responseCode = "200")
    @ApiResponse(responseCode = "404",
            content = @Content(mediaType = TEXT_PLAIN_VALUE))
    @ApiResponse(responseCode = "422",
            content = @Content(mediaType = TEXT_PLAIN_VALUE))
    @JsonView(ArtistViews.ViewWithId.class)
    @PutMapping("/artists/{id}")    //patch - exactly what we want to update, put - everything updates
    public ArtistDto update(@PathVariable Long id,
                            @JsonView(ArtistViews.ViewWithoutId.class) @RequestBody ArtistDto artistDto) {
        Artist artist = fromDto(artistDto);
        artist.setId(id);
        return toDto(artistService.update(artist));
    }

    @Operation(summary = "Delete artist by ID")
    @DeleteMapping("/artists/{id}")
    @ResponseStatus(NO_CONTENT)
    public void deleteById(@PathVariable Long id){
        artistService.deleteById(id);
    }

    @Operation(summary = "Delete all artists who haven't performed for year")
    @DeleteMapping("/old_artists")
    @ResponseStatus(NO_CONTENT)
    public void deleteAllArtistsNotPerformedForAYear() {
        artistService.deleteAllArtistsNotPerformedForAYear();
    }
}
