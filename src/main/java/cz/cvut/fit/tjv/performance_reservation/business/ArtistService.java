package cz.cvut.fit.tjv.performance_reservation.business;

import cz.cvut.fit.tjv.performance_reservation.business.exceptions.DeleteNonExistingEntityException;
import cz.cvut.fit.tjv.performance_reservation.business.exceptions.NonExistingEntityException;
import cz.cvut.fit.tjv.performance_reservation.business.exceptions.NullEntityFieldException;
import cz.cvut.fit.tjv.performance_reservation.dao.ArtistJpaRepository;
import cz.cvut.fit.tjv.performance_reservation.domain.Artist;
import cz.cvut.fit.tjv.performance_reservation.domain.Performance;
import lombok.NonNull;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.lang.reflect.Array;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Service
public class ArtistService extends AbstractCrudService<Long, Artist, ArtistJpaRepository>{
    protected ArtistService(ArtistJpaRepository repository) {
        super(repository);
    }

    @Transactional
    public Collection<Performance> readPerformancesOfArtist(@NonNull Long artId) {
        Artist artist = readById(artId).orElseThrow(NonExistingEntityException::new);
        return artist.getArtistsPerformances();
    }

    @Override
    protected void deleteRelationsById(Long id) {
        Artist artist = readById(id).orElseThrow(DeleteNonExistingEntityException::new);
        artist.getArtistsPerformances()
                .forEach(performance -> performance.getPerformingArtists().remove(artist));
    }
    @Override
    protected void checkNonNullableFields(Artist artist) {
        if (artist.getName() == null){
            throw new NullEntityFieldException();
        }
    }
    @Transactional
    public void deleteAllArtistsNotPerformedForAYear() {
        LocalDateTime dateNowMinusYear = LocalDateTime.now().minusYears(1);
        List<Artist> performedWithinTimePeriod =
                repository.findAllArtistsPerformedAfterDate(dateNowMinusYear);
        List<Artist> allArtists = repository.findAll();
        allArtists.removeAll(performedWithinTimePeriod);
        allArtists.forEach(artist -> deleteById(artist.getId()));
    }
}
