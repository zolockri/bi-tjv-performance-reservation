package cz.cvut.fit.tjv.performance_reservation.business.exceptions;

public class NullEntityFieldException extends RuntimeException {
    public NullEntityFieldException() {}

    public <E> NullEntityFieldException(E entity) {
        super("Null field of entity " + entity);
    }

    public NullEntityFieldException(String s) {
        super(s);
    }
}
