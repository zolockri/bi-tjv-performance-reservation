package cz.cvut.fit.tjv.performance_reservation.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@ToString(onlyExplicitlyIncluded = true)
@NoArgsConstructor
@Entity
public class Performance implements DomainEntity<Long> {
    @ToString.Include
    @EqualsAndHashCode.Include
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ToString.Include
    @Column(nullable = false)
    @JsonFormat(pattern = "dd/MM/yyyy HH:mm:ss")
    private LocalDateTime dateTimeFrom;

    @ToString.Include
    @Column(nullable = false)
    @JsonFormat(pattern = "dd/MM/yyyy HH:mm:ss")
    private LocalDateTime dateTimeTo;

    public Performance(Long id, LocalDateTime dateTimeFrom, LocalDateTime dateTimeTo){
        this.id = id;
        this.dateTimeFrom = dateTimeFrom;
        this.dateTimeTo = dateTimeTo;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "establishment_fk")
    private Establishment establishment;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "performance_artist",
            joinColumns = @JoinColumn(name = "performance_id"),
            inverseJoinColumns = @JoinColumn(name = "artist_id")
    )
    private Set<Artist> performingArtists = new HashSet<>();

    @Override
    public Long getId() {
        return id;
    }

}
