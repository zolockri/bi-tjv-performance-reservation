package cz.cvut.fit.tjv.performance_reservation.domain;

import lombok.*;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@ToString(onlyExplicitlyIncluded = true)
@NoArgsConstructor
@Entity
public class Artist implements DomainEntity<Long> {
    @ToString.Include
    @EqualsAndHashCode.Include
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ToString.Include
    @Column(nullable = false)
    private String name;

    @ToString.Include
    @Column
    private String genre;

    public Artist(Long id, String name, String genre) {
        this.id = id;
        this.name = name;
        this.genre = genre;
    }

    @ManyToMany(mappedBy = "performingArtists", fetch = FetchType.EAGER)
    private Set<Performance> artistsPerformances = new HashSet<>();

    @Override
    public Long getId() {
        return id;
    }
}
