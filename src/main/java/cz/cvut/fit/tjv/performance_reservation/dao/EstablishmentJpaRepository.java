package cz.cvut.fit.tjv.performance_reservation.dao;

import cz.cvut.fit.tjv.performance_reservation.domain.Establishment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EstablishmentJpaRepository extends JpaRepository<Establishment, Long> {
}
