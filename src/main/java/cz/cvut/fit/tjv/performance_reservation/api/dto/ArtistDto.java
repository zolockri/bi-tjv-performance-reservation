package cz.cvut.fit.tjv.performance_reservation.api.dto;

import com.fasterxml.jackson.annotation.JsonView;
import cz.cvut.fit.tjv.performance_reservation.api.views.ArtistViews;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

@Getter
@Setter
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor

public class ArtistDto {
    @Schema(requiredMode = Schema.RequiredMode.REQUIRED)
    @JsonView(ArtistViews.ViewWithId.class)
    private Long id;
    @Schema(requiredMode = Schema.RequiredMode.REQUIRED, example = "Beastie Boys")
    @JsonView(ArtistViews.ViewWithoutId.class)
    private String name;
    @Schema(requiredMode = Schema.RequiredMode.NOT_REQUIRED, example = "hip hop")
    @JsonView(ArtistViews.ViewWithoutId.class)
    private String genre;
}
