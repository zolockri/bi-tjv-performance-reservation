package cz.cvut.fit.tjv.performance_reservation.business;

import cz.cvut.fit.tjv.performance_reservation.business.exceptions.DeleteNonExistingEntityException;
import cz.cvut.fit.tjv.performance_reservation.business.exceptions.NonExistingEntityException;
import cz.cvut.fit.tjv.performance_reservation.business.exceptions.NullEntityFieldException;
import cz.cvut.fit.tjv.performance_reservation.dao.EstablishmentJpaRepository;
import cz.cvut.fit.tjv.performance_reservation.domain.Establishment;
import cz.cvut.fit.tjv.performance_reservation.domain.Performance;
import lombok.NonNull;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Collection;

@Service
public class EstablishmentService extends AbstractCrudService<Long, Establishment, EstablishmentJpaRepository>{

    private final PerformanceService performanceService;
    protected EstablishmentService(EstablishmentJpaRepository repository, PerformanceService performanceService) {
        super(repository);
        this.performanceService = performanceService;
    }

    @Transactional
    public void updatePerformanceToEstablishment(@NonNull Long perfId, @NonNull Long estId){
        Performance performance = performanceService.readById(perfId).orElseThrow(NonExistingEntityException::new);
        Establishment establishment = readById(estId).orElseThrow(NonExistingEntityException::new);
        performance.setEstablishment(establishment);
    }

    @Transactional
    public Collection<Performance> readPerformancesInEstablishment(@NonNull Long estId) {
        Establishment establishment = readById(estId).orElseThrow(NonExistingEntityException::new);
        return establishment.getPerformances();
    }

    @Override
    protected void deleteRelationsById(Long id) {
        Establishment establishment = readById(id).orElseThrow(DeleteNonExistingEntityException::new);
        establishment.getPerformances().forEach(performance -> performance.setEstablishment(null));
    }

    @Override
    protected void checkNonNullableFields(Establishment establishment) {
        if (establishment.getName() == null){
            throw new NullEntityFieldException();
        }
    }


}
