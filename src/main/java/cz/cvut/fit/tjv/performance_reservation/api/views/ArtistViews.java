package cz.cvut.fit.tjv.performance_reservation.api.views;

public class ArtistViews {
    public static class ViewWithoutId {
    }

    public static class ViewWithId extends ViewWithoutId {
    }
}
