package cz.cvut.fit.tjv.performance_reservation.dao;

import cz.cvut.fit.tjv.performance_reservation.domain.Artist;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface ArtistJpaRepository extends JpaRepository<Artist, Long> {
    @Query("SELECT DISTINCT p.performingArtists " +
            "FROM Performance p " +
            "WHERE p.dateTimeTo > :date" )
    List<Artist> findAllArtistsPerformedAfterDate(@Param("date") LocalDateTime date);

}
