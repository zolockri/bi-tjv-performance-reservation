package cz.cvut.fit.tjv.performance_reservation.api.converter;

import cz.cvut.fit.tjv.performance_reservation.api.dto.PerformanceDto;
import cz.cvut.fit.tjv.performance_reservation.domain.Performance;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class PerformanceConverter {

    public static Performance fromDto(PerformanceDto dto) {
        return new Performance(dto.getId(), dto.getDateTimeFrom(), dto.getDateTimeTo());
    }

    public static PerformanceDto toDto(Performance performance) {
        return new PerformanceDto(performance.getId(), performance.getDateTimeFrom(), performance.getDateTimeTo());
    }

    public static Collection<PerformanceDto> toDtoMany(Collection<Performance> performances) {
        List<PerformanceDto> performanceDtos = new ArrayList<>();
        performances.forEach(performance -> performanceDtos.add(toDto(performance)));
        return performanceDtos;
    }

}
