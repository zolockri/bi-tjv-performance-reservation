package cz.cvut.fit.tjv.performance_reservation.business.exceptions;

public class DeleteNonExistingEntityException extends RuntimeException {
    public DeleteNonExistingEntityException(){
        super("Deleting non-existing entity");
    }
}
