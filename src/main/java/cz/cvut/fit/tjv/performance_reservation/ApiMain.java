package cz.cvut.fit.tjv.performance_reservation;

import org.springframework.boot.autoconfigure.SpringBootApplication;

import static org.springframework.boot.SpringApplication.run;


@SpringBootApplication
public class ApiMain {
    public static void main(String[] args) {
        run(ApiMain.class, args);
    }
}