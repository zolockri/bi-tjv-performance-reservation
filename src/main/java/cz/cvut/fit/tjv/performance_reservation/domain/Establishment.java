package cz.cvut.fit.tjv.performance_reservation.domain;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@ToString(onlyExplicitlyIncluded = true)
@NoArgsConstructor
@Entity
public class Establishment implements DomainEntity<Long> {
    @ToString.Include
    @EqualsAndHashCode.Include
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ToString.Include
    @Column(nullable = false)
    private String name;

    @ToString.Include
    private String address;

    @ToString.Include
    @Column
    private String type;

    public Establishment(Long id, String name, String address, String type){
        this.id = id;
        this.name = name;
        this.address = address;
        this.type = type;
    }

    @OneToMany(mappedBy = "establishment", fetch = FetchType.EAGER)
    private Set<Performance> performances = new HashSet<>();

    @Override
    public Long getId() {
        return id;
    }
}
