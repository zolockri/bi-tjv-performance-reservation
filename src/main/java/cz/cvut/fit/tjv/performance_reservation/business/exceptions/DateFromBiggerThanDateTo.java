package cz.cvut.fit.tjv.performance_reservation.business.exceptions;

public class DateFromBiggerThanDateTo extends RuntimeException {
    public DateFromBiggerThanDateTo() {super("Date-time from is bigger than date-time to");}
}
