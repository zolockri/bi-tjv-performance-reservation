package cz.cvut.fit.tjv.performance_reservation.api.dto;


import com.fasterxml.jackson.annotation.JsonView;
import cz.cvut.fit.tjv.performance_reservation.api.views.ArtistViews;
import cz.cvut.fit.tjv.performance_reservation.api.views.EstablishmentViews;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

@Getter
@Setter
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor

public class EstablishmentDto {
    @Schema(requiredMode = Schema.RequiredMode.REQUIRED)
    @JsonView(EstablishmentViews.ViewWithId.class)
    private Long id;
    @Schema(requiredMode = Schema.RequiredMode.REQUIRED, example = "Forum Karlín")
    @JsonView(EstablishmentViews.ViewWithoutId.class)
    private String name;
    @Schema(requiredMode = Schema.RequiredMode.NOT_REQUIRED, example = "Pernerova 51, 186 00 Praha 8-Karlín")
    @JsonView(EstablishmentViews.ViewWithoutId.class)
    private String address;
    @Schema(requiredMode = Schema.RequiredMode.NOT_REQUIRED, example = "Event venue")
    @JsonView(EstablishmentViews.ViewWithoutId.class)
    private String type;
}

