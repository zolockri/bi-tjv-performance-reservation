package cz.cvut.fit.tjv.performance_reservation.api.converter;

import cz.cvut.fit.tjv.performance_reservation.api.dto.EstablishmentDto;
import cz.cvut.fit.tjv.performance_reservation.domain.Establishment;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class EstablishmentConverter {
    public static Establishment fromDto(EstablishmentDto dto) {
        return new Establishment(dto.getId(), dto.getName(), dto.getAddress(), dto.getType());
    }

    public static EstablishmentDto toDto (Establishment establishment) {
        return new EstablishmentDto(establishment.getId(), establishment.getName(), establishment.getAddress(),
                establishment.getType());
    }

    public static Collection<EstablishmentDto> toDtoMany (Collection<Establishment> establishments){
        List<EstablishmentDto> establishmentDtos = new ArrayList<>();
        establishments.forEach(establishment -> establishmentDtos.add(toDto(establishment)));
        return establishmentDtos;
    }
}
