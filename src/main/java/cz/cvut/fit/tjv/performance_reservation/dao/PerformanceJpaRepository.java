package cz.cvut.fit.tjv.performance_reservation.dao;

import cz.cvut.fit.tjv.performance_reservation.domain.Performance;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PerformanceJpaRepository extends JpaRepository<Performance, Long> {
}
