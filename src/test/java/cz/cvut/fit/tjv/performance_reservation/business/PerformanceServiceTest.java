package cz.cvut.fit.tjv.performance_reservation.business;

import cz.cvut.fit.tjv.performance_reservation.business.exceptions.*;
import cz.cvut.fit.tjv.performance_reservation.dao.PerformanceJpaRepository;
import cz.cvut.fit.tjv.performance_reservation.domain.Artist;
import cz.cvut.fit.tjv.performance_reservation.domain.Performance;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@SpringBootTest
public class PerformanceServiceTest {
    @Autowired
    private PerformanceService performanceService;

    @MockBean
    private PerformanceJpaRepository performanceRepository;

    private final LocalDateTime dateTime1 = LocalDateTime.of(2022, 3, 1, 14, 0);
    private final LocalDateTime dateTime2 = LocalDateTime.of(2022, 3, 1, 20, 0);
    private final LocalDateTime dateTime3 = LocalDateTime.of(2022, 4, 1, 14, 0);
    private final LocalDateTime dateTime4 = LocalDateTime.of(2022, 4, 1, 20, 0);
    private final LocalDateTime dateTime5 = LocalDateTime.of(2022, 5, 1, 14, 0);
    private final LocalDateTime dateTime6 = LocalDateTime.of(2022, 5, 1, 20, 0);
    private final LocalDateTime dateTime7 = LocalDateTime.of(2022, 3, 2, 14, 0);
    private final LocalDateTime dateTime8 = LocalDateTime.of(2022, 3, 1, 20, 0);
    private final LocalDateTime dateTime9 = LocalDateTime.of(2025, 12, 31, 16, 42);
    private final LocalDateTime dateTime10 = LocalDateTime.of(2026, 12, 31, 16, 42);

    @Test
    public void testCreateSuccess() {
        Performance pCreated = new Performance(null, dateTime9, dateTime10);
        Performance pCreatedWithId = new Performance(1L, dateTime9, dateTime10);

        Mockito.when(performanceRepository.save(pCreated)).thenReturn(pCreatedWithId);
        Performance res = performanceService.create(pCreated);
        Assertions.assertIterableEquals(List.of(pCreatedWithId.getId(), pCreatedWithId.getDateTimeFrom(), pCreatedWithId.getDateTimeTo()),
                List.of(res.getId(), res.getDateTimeFrom(), res.getDateTimeTo()));
        Mockito.verify(performanceRepository).save(pCreated);
    }

    @Test
    public void testCreateWithNullFields() {
        Performance pCreatedWithNull = new Performance(1L, null, dateTime1);
        Assertions.assertThrows(NullEntityFieldException.class, () -> performanceService.create(pCreatedWithNull));
    }

    @Test
    public void testCreateIllegalTimeOrder() {
        Performance pCreatedWithIllegalTimeOrder = new Performance(1L, dateTime10, dateTime9);
        Assertions.assertThrows(DateFromBiggerThanDateTo.class, () -> performanceService.create(pCreatedWithIllegalTimeOrder));
    }

    @Test
    public void testCreateSameTime() {
        Performance pCreatedWithIllegalTimeOrder = new Performance(1L, dateTime10, dateTime10);
        Assertions.assertThrows(DateFromBiggerThanDateTo.class, () -> performanceService.create(pCreatedWithIllegalTimeOrder));
    }

    @Test
    public void testReadByIdSuccess() {
        Performance p1 = new Performance(1L, dateTime1, dateTime2);
        Performance p2 = new Performance(2L, dateTime3, dateTime4);
        Performance p3 = new Performance(3L, dateTime5, dateTime6);

        Mockito.when(performanceRepository.findById(1L)).thenReturn(Optional.of(p1));
        Mockito.when(performanceRepository.findById(2L)).thenReturn(Optional.of(p2));
        Mockito.when(performanceRepository.findById(3L)).thenReturn(Optional.of(p3));

        Performance res1 = performanceService.readById(1L).orElse(null);
        Performance res2 = performanceService.readById(2L).orElse(null);
        Performance res3 = performanceService.readById(3L).orElse(null);

        Assertions.assertNotNull(res1);
        Assertions.assertIterableEquals(List.of(p1.getId(), p1.getDateTimeFrom(), p1.getDateTimeTo()),
                List.of(res1.getId(), res1.getDateTimeFrom(), res1.getDateTimeTo()));
        Assertions.assertNotNull(res2);
        Assertions.assertIterableEquals(List.of(p2.getId(), p2.getDateTimeFrom(), p2.getDateTimeTo()),
                List.of(res2.getId(), res2.getDateTimeFrom(), res2.getDateTimeTo()));
        Assertions.assertNotNull(res3);
        Assertions.assertIterableEquals(List.of(p3.getId(), p3.getDateTimeFrom(), p3.getDateTimeTo()),
                List.of(res3.getId(), res3.getDateTimeFrom(), res3.getDateTimeTo()));

        Mockito.verify(performanceRepository).findById(p1.getId());
        Mockito.verify(performanceRepository).findById(p2.getId());
        Mockito.verify(performanceRepository).findById(p3.getId());
    }

    @Test
    public void testReadByIdNonExisting() {
        Mockito.when(performanceRepository.findById(775L)).thenReturn(Optional.empty());
        Assertions.assertEquals(Optional.empty(), performanceService.readById(775L));
    }

    @Test
    public void testReadAll() {
        Performance p1 = new Performance(1L, dateTime1, dateTime2);
        Performance p2 = new Performance(2L, dateTime3, dateTime4);
        Performance p3 = new Performance(3L, dateTime5, dateTime6);
        List<Performance> performances = List.of(p1, p2, p3);

        Mockito.when(performanceRepository.findAll()).thenReturn(performances);

        List<Performance> results = performanceService.readAll().stream().toList();

        Assertions.assertIterableEquals(
                List.of(performances.get(0).getId(),
                        performances.get(0).getDateTimeFrom(),
                        performances.get(0).getDateTimeTo()),
                List.of(results.get(0).getId(),
                        results.get(0).getDateTimeFrom(),
                        results.get(0).getDateTimeTo()));

        Assertions.assertIterableEquals(
                List.of(performances.get(1).getId(),
                        performances.get(1).getDateTimeFrom(),
                        performances.get(1).getDateTimeTo()),
                List.of(results.get(1).getId(),
                        results.get(1).getDateTimeFrom(),
                        results.get(1).getDateTimeTo()));

        Assertions.assertIterableEquals(
                List.of(performances.get(2).getId(),
                        performances.get(2).getDateTimeFrom(),
                        performances.get(2).getDateTimeTo()),
                List.of(results.get(2).getId(),
                        results.get(2).getDateTimeFrom(),
                        results.get(2).getDateTimeTo()));

        Mockito.verify(performanceRepository).findAll();
    }

    @Test
    public void testUpdateSuccess() {
        Performance pToUpdate = new Performance(1L, dateTime2, dateTime3);
        Performance pUpdated = new Performance(1L, dateTime2, dateTime7);
        Mockito.when(performanceRepository.save(pUpdated)).thenReturn(pUpdated);
        Mockito.when(performanceRepository.findById(1L)).thenReturn(Optional.of(pToUpdate));
        Performance res = performanceService.update(pUpdated);

        Assertions.assertIterableEquals(List.of(pUpdated.getId(), pUpdated.getDateTimeFrom(), pUpdated.getDateTimeTo()),
                List.of(res.getId(), res.getDateTimeFrom(), res.getDateTimeTo()));
    }

    @Test
    public void testUpdateWithICFail() {
        LocalDateTime oxxxyTime1 = LocalDateTime.of(2023, 3, 17, 20, 0);
        LocalDateTime oxxxyTime2 = LocalDateTime.of(2023, 3, 18, 0, 0);

        LocalDateTime oxxxyTime3 = LocalDateTime.of(2023, 3, 24, 20, 0);
        LocalDateTime oxxxyTime4 = LocalDateTime.of(2023, 3, 25, 0, 0);

        LocalDateTime oxxxyTime5 = LocalDateTime.of(2023, 3, 24, 17, 0);
        LocalDateTime oxxxyTime6 = LocalDateTime.of(2023, 3, 24, 23, 0);

        LocalDateTime morgTime1 = LocalDateTime.of(2022, 10, 15, 20, 0);
        LocalDateTime morgTime2 = LocalDateTime.of(2022, 10, 15, 20, 0, 0, 1);


        Artist morgenstern = new Artist(1L, "morgenstern", "rap");
        Artist oxxxy = new Artist(2L, "Oxxxymiron", "hip-hop");

        Performance oxxxy1 = new Performance(3L, oxxxyTime1, oxxxyTime2);
        Performance oxxxy2 = new Performance(4L, oxxxyTime3, oxxxyTime4);

        Performance morgen = new Performance(5L, morgTime1, morgTime2);

        oxxxy1.setPerformingArtists(Set.of(oxxxy));
        oxxxy2.setPerformingArtists(Set.of(oxxxy));
        morgen.setPerformingArtists(Set.of(morgenstern));

        oxxxy.setArtistsPerformances(Set.of(oxxxy1, oxxxy2));
        morgenstern.setArtistsPerformances(Set.of(morgen));

        Performance oxxxy3 = new Performance(3L, oxxxyTime5, oxxxyTime6);

        Mockito.when(performanceRepository.findById(3L)).thenReturn(Optional.of(oxxxy1));
        Assertions.assertThrows(PerformanceTimeIntersectionException.class, () -> performanceService.update(oxxxy3));
    }

    @Test
    public void testUpdateNonExisting() {
        Performance pUpdated = new Performance(775L, dateTime2, dateTime7);
        Mockito.when(performanceRepository.findById(775L)).thenReturn(Optional.empty());
        Assertions.assertThrows(NonExistingEntityException.class, () -> performanceService.update(pUpdated));
    }

    @Test
    public void testUpdateWithNullFields() {
        Performance pToUpdate = new Performance(1L, dateTime2, dateTime3);
        Mockito.when(performanceRepository.findById(1L)).thenReturn(Optional.of(pToUpdate));
        Performance pUpdatedWithNull = new Performance(1L, null, dateTime1);
        Assertions.assertThrows(NullEntityFieldException.class, () -> performanceService.create(pUpdatedWithNull));
    }

    @Test
    public void testUpdateWithIllegalTimeOrder() {
        Performance pToUpdate = new Performance(1L, dateTime2, dateTime3);
        Mockito.when(performanceRepository.findById(1L)).thenReturn(Optional.of(pToUpdate));
        Performance pUpdatedWithIllegalTimeOrder = new Performance(1L, dateTime2, dateTime1);
        Assertions.assertThrows(DateFromBiggerThanDateTo.class, () -> performanceService.create(pUpdatedWithIllegalTimeOrder));
    }

    @Test
    public void testUpdateWithSameTime() {
        Performance pToUpdate = new Performance(1L, dateTime2, dateTime3);
        Mockito.when(performanceRepository.findById(1L)).thenReturn(Optional.of(pToUpdate));
        Performance pUpdatedWithIllegalTimeOrder = new Performance(1L, dateTime2, dateTime2);
        Assertions.assertThrows(DateFromBiggerThanDateTo.class, () -> performanceService.create(pUpdatedWithIllegalTimeOrder));
    }

    @Test
    public void testDeleteByIdSuccess() {
        Mockito.when(performanceRepository.existsById(1L)).thenReturn(Boolean.TRUE);
        performanceService.deleteById(1L);
        Mockito.verify(performanceRepository).deleteById(1L);
    }

    @Test
    public void testDeleteByIdNonExisting() {
        Mockito.when(performanceRepository.existsById(775L)).thenReturn(Boolean.FALSE);
        Assertions.assertThrows(DeleteNonExistingEntityException.class, () -> performanceService.deleteById(775L));
    }
}