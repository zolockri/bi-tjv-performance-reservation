package cz.cvut.fit.tjv.performance_reservation.api.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import cz.cvut.fit.tjv.performance_reservation.api.converter.EstablishmentConverter;
import cz.cvut.fit.tjv.performance_reservation.api.dto.EstablishmentDto;
import cz.cvut.fit.tjv.performance_reservation.business.EstablishmentService;
import cz.cvut.fit.tjv.performance_reservation.business.exceptions.DeleteNonExistingEntityException;
import cz.cvut.fit.tjv.performance_reservation.business.exceptions.NonExistingEntityException;
import cz.cvut.fit.tjv.performance_reservation.business.exceptions.NullEntityFieldException;
import cz.cvut.fit.tjv.performance_reservation.domain.Establishment;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;
import java.util.Optional;

import static cz.cvut.fit.tjv.performance_reservation.api.converter.EstablishmentConverter.toDto;
import static org.mockito.Mockito.doThrow;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@WebMvcTest(EstablishmentController.class)
public class EstablishmentControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private EstablishmentService establishmentService;


    @Test
    public void testCreateSuccess() throws Exception {
        Establishment eCreated = new Establishment(null, "test", "test", "test");
        Establishment eCreatedWithId = new Establishment(1L, "test", "test", "test");
        Mockito.when(establishmentService.create(eCreated)).thenReturn(eCreatedWithId);

        mockMvc.perform(post("/establishments")
                        .contentType(APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(toDto(eCreated))))
                .andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(toDto(eCreatedWithId))));
    }

    @Test
    public void testCreateWithNullFields() throws Exception {
        Establishment eWithNullFields = new Establishment(null, null, null, null);
        Mockito.when(establishmentService.create(eWithNullFields)).thenThrow(NullEntityFieldException.class);
        mockMvc.perform(post("/establishments")
                        .contentType(APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(toDto(eWithNullFields))))
                .andExpect(status().isUnprocessableEntity())
                .andExpect(content().string("Entity null field"));
    }

    @Test
    public void testReadSingleEntity() throws Exception {
        Establishment e1 = new Establishment(1L, "test1", "test1", "test");
        Establishment e2 = new Establishment(2L, "test2", "test2", "test");
        Establishment e3 = new Establishment(3L, "test3", "test3", "test");

        Mockito.when(establishmentService.readById(1L)).thenReturn(Optional.of(e1));
        Mockito.when(establishmentService.readById(2L)).thenReturn(Optional.of(e2));
        Mockito.when(establishmentService.readById(3L)).thenReturn(Optional.of(e3));

        mockMvc.perform(get("/establishments/1"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_JSON))
                .andExpect(content().json(objectMapper.writeValueAsString(toDto(e1))));
        mockMvc.perform(get("/establishments/2"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_JSON))
                .andExpect(content().json(objectMapper.writeValueAsString(toDto(e2))));
        mockMvc.perform(get("/establishments/3"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_JSON))
                .andExpect(content().json(objectMapper.writeValueAsString(toDto(e3))));
    }

    @Test
    public void testReadNonExistingEntity() throws Exception {
        Mockito.when(establishmentService.readById(775L)).thenReturn(Optional.empty());
        mockMvc.perform(get("/establishments/775"))
                .andExpect(status().isNotFound())
                .andExpect(content().string("Entity not found"));
    }

    @Test
    public void testReadAll() throws Exception {
        Establishment e1 = new Establishment(1L, "test", "test1", "test");
        Establishment e2 = new Establishment(2L, "test", "test2", "test");
        Establishment e3 = new Establishment(3L, "test", "test3", "test");

        List<Establishment> establishments = List.of(e1, e2, e3);

        Mockito.when(establishmentService.readAll()).thenReturn(establishments);

        List<EstablishmentDto> establishmentsDto = establishments.stream().map(EstablishmentConverter::toDto).toList();
        mockMvc.perform(get("/establishments"))
                .andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(establishmentsDto)));
    }

    @Test
    public void testUpdateSuccess() throws Exception {
        Establishment establishmentUpdated = new Establishment(1L, "lol", null, null);
        Mockito.when(establishmentService.update(establishmentUpdated)).thenReturn(establishmentUpdated);
        mockMvc.perform(put("/establishments/1")
                        .contentType(APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(toDto(establishmentUpdated))))
                .andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_JSON))
                .andExpect(content().json(objectMapper.writeValueAsString(toDto(establishmentUpdated))));
    }

    @Test
    public void testUpdateWithNullFields() throws Exception {
        Establishment establishmentUpdatedWithNullFields = new Establishment(1L, null, null, null);
        Mockito.when(establishmentService.update(establishmentUpdatedWithNullFields)).thenThrow(NullEntityFieldException.class);
        mockMvc.perform(put("/establishments/1")
                        .contentType(APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(toDto(establishmentUpdatedWithNullFields))))
                .andExpect(status().isUnprocessableEntity())
                .andExpect(content().string("Entity null field"));
    }

    @Test
    public void testUpdateNonExistingEntity() throws Exception {
        Establishment establishmentUpdateNonExisting = new Establishment(12345L, "blabla", null, null);
        Mockito.when(establishmentService.update(establishmentUpdateNonExisting)).thenThrow(NonExistingEntityException.class);
        mockMvc.perform(put("/establishments/12345")
                        .contentType(APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(toDto(establishmentUpdateNonExisting))))
                .andExpect(status().isNotFound())
                .andExpect(content().string("Entity not found"));
    }

    @Test
    public void testDeleteExisting() throws Exception {
        mockMvc.perform(delete("/establishments/1"))
                .andExpect(status().isNoContent())
                .andExpect(content().string(""));
    }

    @Test
    public void testDeleteNonExisting() throws Exception {
        doThrow(DeleteNonExistingEntityException.class).when(establishmentService).deleteById(12345L);
        mockMvc.perform(delete("/establishments/12345"))
                .andExpect(status().isNoContent())
                .andExpect(content().string(""));
    }

}