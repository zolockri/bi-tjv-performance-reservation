package cz.cvut.fit.tjv.performance_reservation.api.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import cz.cvut.fit.tjv.performance_reservation.api.converter.PerformanceConverter;
import cz.cvut.fit.tjv.performance_reservation.api.dto.PerformanceDto;
import cz.cvut.fit.tjv.performance_reservation.business.PerformanceService;
import cz.cvut.fit.tjv.performance_reservation.business.exceptions.DateFromBiggerThanDateTo;
import cz.cvut.fit.tjv.performance_reservation.business.exceptions.DeleteNonExistingEntityException;
import cz.cvut.fit.tjv.performance_reservation.business.exceptions.NonExistingEntityException;
import cz.cvut.fit.tjv.performance_reservation.business.exceptions.NullEntityFieldException;
import cz.cvut.fit.tjv.performance_reservation.domain.Artist;
import cz.cvut.fit.tjv.performance_reservation.domain.Performance;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import static cz.cvut.fit.tjv.performance_reservation.api.converter.PerformanceConverter.toDto;
import static org.mockito.Mockito.doThrow;
import static org.springframework.http.MediaType.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@WebMvcTest(PerformanceController.class)
public class PerformanceControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;
    @MockBean
    private PerformanceService performanceService;
    private final LocalDateTime dateTime1 = LocalDateTime.of(2022, 3, 1, 14, 0);
    private final LocalDateTime dateTime2 = LocalDateTime.of(2022, 3, 1, 20, 0);
    private final LocalDateTime dateTime3 = LocalDateTime.of(2022, 4, 1, 14, 0);
    private final LocalDateTime dateTime4 = LocalDateTime.of(2022, 4, 1, 20, 0);
    private final LocalDateTime dateTime5 = LocalDateTime.of(2022, 5, 1, 14, 0);
    private final LocalDateTime dateTime6 = LocalDateTime.of(2022, 5, 1, 20, 0);
    private final LocalDateTime dateTime7 = LocalDateTime.of(2022, 3, 2, 14, 0);
    private final LocalDateTime dateTime8 = LocalDateTime.of(2022, 3, 1, 20, 0);
    private final LocalDateTime dateTime9 = LocalDateTime.of(2025, 12, 31, 16, 42);
    private final LocalDateTime dateTime10 = LocalDateTime.of(2026, 12, 31, 16, 42);

    private final Artist a1 = new Artist(5L, "band1", "rock");
    private final Artist a2 = new Artist(6L, "band2", "rock");
    private final Artist a3 = new Artist(7L, "band3", "rock");
    private final List<Artist> a1s = List.of(a1, a2);
    private final List<Artist> a2s = List.of(a3);
    private final List<Artist> a3s = List.of();

    @Test
    public void testCreateSuccess() throws Exception {
        Performance p4Created = new Performance(null, dateTime9, dateTime10);
        Performance p4CreatedWithId = new Performance(4L, dateTime9, dateTime10);

        Mockito.when(performanceService.create(p4Created)).thenReturn(p4CreatedWithId);
        mockMvc.perform(post("/performances")
                        .contentType(APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(toDto(p4Created))))
                .andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_JSON))
                .andExpect(content().json(objectMapper.writeValueAsString(toDto(p4CreatedWithId))));
    }

    @Test
    public void testCreateWithNullFields() throws Exception {
        Performance p5WithNull = new Performance(null, null, dateTime10);
        Mockito.when(performanceService.create(p5WithNull)).thenThrow(NullEntityFieldException.class);
        mockMvc.perform(post("/performances")
                        .contentType(APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(toDto(p5WithNull))))
                .andExpect(status().isUnprocessableEntity())
                .andExpect(content().string("Entity null field"));
    }

    @Test
    public void testCreateWithInvalidTimeOrder() throws Exception {
        Performance p6WithWrongTimeLine = new Performance(null, dateTime10, dateTime9);
        Mockito.when(performanceService.create(p6WithWrongTimeLine)).thenThrow(DateFromBiggerThanDateTo.class);
        mockMvc.perform(post("/performances")
                        .contentType(APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(toDto(p6WithWrongTimeLine))))
                .andExpect(status().isUnprocessableEntity())
                .andExpect(content().string("Wrong date-time line: start time must be before end time"));
    }

    @Test
    public void testReadSingleEntity() throws Exception {
        Performance p1 = new Performance(1L, dateTime1, dateTime2);
        Performance p2 = new Performance(2L, dateTime3, dateTime4);
        Performance p3 = new Performance(3L, dateTime5, dateTime6);

        Mockito.when(performanceService.readById(1L)).thenReturn(Optional.of(p1));
        Mockito.when(performanceService.readById(2L)).thenReturn(Optional.of(p2));
        Mockito.when(performanceService.readById(3L)).thenReturn(Optional.of(p3));

        mockMvc.perform(get("/performances/1"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_JSON))
                .andExpect(content().json(objectMapper.writeValueAsString(toDto(p1))));
        mockMvc.perform(get("/performances/2"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_JSON))
                .andExpect(content().json(objectMapper.writeValueAsString(toDto(p2))));
        mockMvc.perform(get("/performances/3"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_JSON))
                .andExpect(content().json(objectMapper.writeValueAsString(toDto(p3))));
    }

    @Test
    public void testReadNonExistingEntity() throws Exception {
        Mockito.when(performanceService.readById(775L)).thenReturn(Optional.empty());
        mockMvc.perform(get("/performances/775"))
                .andExpect(status().isNotFound())
                .andExpect(content().string("Entity not found"));
    }

    @Test
    public void testReadAll() throws Exception {
        Performance p1 = new Performance(1L, dateTime1, dateTime2);
        Performance p2 = new Performance(2L, dateTime3, dateTime4);
        Performance p3 = new Performance(3L, dateTime5, dateTime6);

        List<Performance> performances = List.of(p1, p2, p3);

        Mockito.when(performanceService.readAll()).thenReturn(performances);

        List<PerformanceDto> psDto = performances.stream().map(PerformanceConverter::toDto).toList();
        mockMvc.perform(get("/performances"))
                .andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(psDto)));
    }

    @Test
    public void testUpdateSuccess() throws Exception {
        Performance p1Updated = new Performance(1L, dateTime7, dateTime8);
        Mockito.when(performanceService.update(p1Updated)).thenReturn(p1Updated);
        mockMvc.perform(put("/performances/1")
                        .contentType(APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(toDto(p1Updated))))
                .andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_JSON))
                .andExpect(content().json(objectMapper.writeValueAsString(toDto(p1Updated))));
    }

    @Test
    public void testUpdateWithInvalidTimeOrder() throws Exception {
        Performance p1UpdateWrongTimeOrder = new Performance(1L, dateTime4, dateTime3);
        Mockito.when(performanceService.update(p1UpdateWrongTimeOrder)).thenThrow(DateFromBiggerThanDateTo.class);
        mockMvc.perform(put("/performances/1")
                        .contentType(APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(toDto(p1UpdateWrongTimeOrder))))
                .andExpect(status().isUnprocessableEntity())
                .andExpect(content().string("Wrong date-time line: start time must be before end time"));
    }

    @Test
    public void testUpdateWithNullFields() throws Exception {
        Performance p7UpdateWithNullFields = new Performance(1L, null, null);
        Mockito.when(performanceService.update(p7UpdateWithNullFields)).thenThrow(NullEntityFieldException.class);
        mockMvc.perform(put("/performances/1")
                        .contentType(APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(toDto(p7UpdateWithNullFields))))
                .andExpect(status().isUnprocessableEntity())
                .andExpect(content().string("Entity null field"));
    }

    @Test
    public void testUpdateNonExistingEntity() throws Exception {
        Performance p8UpdateNonExisting = new Performance(12345L, dateTime1, dateTime2);
        Mockito.when(performanceService.update(p8UpdateNonExisting)).thenThrow(NonExistingEntityException.class);
        mockMvc.perform(put("/performances/12345")
                        .contentType(APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(toDto(p8UpdateNonExisting))))
                .andExpect(status().isNotFound())
                .andExpect(content().string("Entity not found"));
    }

    @Test
    public void testDeleteExisting() throws Exception {
        mockMvc.perform(delete("/performances/1"))
                .andExpect(status().isNoContent())
                .andExpect(content().string(""));
    }
    @Test
    public void testDeleteNonExisting() throws Exception {
        doThrow(DeleteNonExistingEntityException.class).when(performanceService).deleteById(12345L);
        mockMvc.perform(delete("/performances/12345"))
                .andExpect(status().isNoContent())
                .andExpect(content().string(""));
    }
}