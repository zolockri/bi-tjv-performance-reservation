package cz.cvut.fit.tjv.performance_reservation.app;

import cz.cvut.fit.tjv.performance_reservation.api.controller.ArtistController;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class ApiMainTest {
    @Autowired
    ArtistController artistController;

    @Test
    public void contextLoadsTests() {
        //test ze di dokazal z toho udelat ctrl ktery chceme
        Assertions.assertThat(artistController).isNotNull(); //pro vsechny pridame pole instanci ktere chceme otestovat
    }
}
