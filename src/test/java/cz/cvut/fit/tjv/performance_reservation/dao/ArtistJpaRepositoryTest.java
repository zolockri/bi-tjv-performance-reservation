package cz.cvut.fit.tjv.performance_reservation.dao;

import cz.cvut.fit.tjv.performance_reservation.domain.Artist;
import cz.cvut.fit.tjv.performance_reservation.domain.Performance;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

@DataJpaTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class ArtistJpaRepositoryTest {
    @Autowired
    ArtistJpaRepository artistJpaRepository;

    @Autowired
    PerformanceJpaRepository performanceJpaRepository;

    private final Artist a1 = new Artist(1L, "test1", "test1");
    private final Artist a2 = new Artist(2L, "test2", "test2");
    private final Artist a3 = new Artist(3L, "test3", "test3");

    private final List<Artist> artists = List.of(a1, a2, a3);

    private final LocalDateTime dateTime1 = LocalDateTime.of(2021, 3, 1, 14, 0);
    private final LocalDateTime dateTime2 = LocalDateTime.of(2021, 3, 1, 20, 0);
    private final LocalDateTime dateTime7 = LocalDateTime.of(2019, 3, 2, 14, 0);
    private final LocalDateTime dateTime8 = LocalDateTime.of(2019, 3, 1, 20, 0);

    private final LocalDateTime dateTime3 = LocalDateTime.of(2020, 4, 1, 14, 0);
    private final LocalDateTime dateTime4 = LocalDateTime.of(2020, 4, 1, 20, 0);
    private final LocalDateTime dateTime5 = LocalDateTime.of(2022, 5, 1, 14, 0);
    private final LocalDateTime dateTime6 = LocalDateTime.of(2022, 5, 1, 20, 0);


    private final Performance p1 = new Performance(5L, dateTime1, dateTime2);
    private final Performance p2 = new Performance(6L, dateTime3, dateTime4);
    private final Performance p3 = new Performance(7L, dateTime5, dateTime6);
    private final Performance p4 = new Performance(8L, dateTime7, dateTime8);
    private final List<Performance> performances = List.of(p1, p2, p3, p4);

    @BeforeAll
    void setUp() {
        artistJpaRepository.saveAll(artists);
        performanceJpaRepository.saveAll(performances);

        p1.setPerformingArtists(Set.of(a1));
        p4.setPerformingArtists(Set.of(a1));
        p2.setPerformingArtists(Set.of(a2));
        p3.setPerformingArtists(Set.of(a3));

        performanceJpaRepository.saveAll(performances);
    }

    @Test
    public void testFindAllArtistsPerformedAfterDate() {

        LocalDateTime dateTimeTest1 = LocalDateTime.of(2023, 3, 1, 20, 0);
        LocalDateTime dateTimeTest2 = LocalDateTime.of(2022, 1, 1, 20, 0);
        LocalDateTime dateTimeTest3 = LocalDateTime.of(2021, 1, 1, 20, 0);

        Assertions.assertEquals(0, artistJpaRepository.findAllArtistsPerformedAfterDate(dateTimeTest1).size());
        Assertions.assertEquals(List.of(), artistJpaRepository.findAllArtistsPerformedAfterDate(dateTimeTest1));
        Assertions.assertEquals(1, artistJpaRepository.findAllArtistsPerformedAfterDate(dateTimeTest2).size());
        Assertions.assertEquals(List.of(a3), artistJpaRepository.findAllArtistsPerformedAfterDate(dateTimeTest2));
        Assertions.assertEquals(2, artistJpaRepository.findAllArtistsPerformedAfterDate(dateTimeTest3).size());
        Assertions.assertEquals(List.of(a1, a3), artistJpaRepository.findAllArtistsPerformedAfterDate(dateTimeTest3));
    }
}