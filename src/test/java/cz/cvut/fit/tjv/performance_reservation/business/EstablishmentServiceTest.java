package cz.cvut.fit.tjv.performance_reservation.business;

import cz.cvut.fit.tjv.performance_reservation.business.exceptions.DeleteNonExistingEntityException;
import cz.cvut.fit.tjv.performance_reservation.business.exceptions.NonExistingEntityException;
import cz.cvut.fit.tjv.performance_reservation.business.exceptions.NullEntityFieldException;
import cz.cvut.fit.tjv.performance_reservation.dao.EstablishmentJpaRepository;
import cz.cvut.fit.tjv.performance_reservation.domain.Establishment;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.List;
import java.util.Optional;

@SpringBootTest
public class EstablishmentServiceTest {
    @Autowired
    private EstablishmentService establishmentService;

    @MockBean
    private EstablishmentJpaRepository establishmentRepository;

    @MockBean
    private PerformanceService performanceService;

    @Test
    public void testCreateSuccess() {
        Establishment eCreated = new Establishment(null, "test", "test", "test");
        Establishment eCreatedWithId = new Establishment(1L, "test", "test", "test");

        Mockito.when(establishmentRepository.save(eCreated)).thenReturn(eCreatedWithId);
        Establishment res = establishmentService.create(eCreated);

        Assertions.assertIterableEquals(List.of(eCreatedWithId.getId(),
                                                eCreatedWithId.getName(),
                                                eCreatedWithId.getAddress(),
                                                eCreatedWithId.getType()),
                                        List.of(res.getId(),
                                                res.getName(),
                                                res.getAddress(),
                                                res.getType()));

        Mockito.verify(establishmentRepository).save(eCreated);
    }

    @Test
    public void testCreateWithNullFields() {
        Establishment eCreatedWithNull = new Establishment(1L, null, "test", "test");
        Assertions.assertThrows(NullEntityFieldException.class, () -> establishmentService.create(eCreatedWithNull));
    }

    @Test
    public void testReadByIdSuccess() {
        Establishment e1 = new Establishment(1L, "dateTime1", "dateTime2", "test");
        Establishment e2 = new Establishment(2L, "dateTime3", "dateTime4", "test");
        Establishment e3 = new Establishment(3L, "dateTime5", "dateTime6", "test");

        Mockito.when(establishmentRepository.findById(1L)).thenReturn(Optional.of(e1));
        Mockito.when(establishmentRepository.findById(2L)).thenReturn(Optional.of(e2));
        Mockito.when(establishmentRepository.findById(3L)).thenReturn(Optional.of(e3));

        Establishment res1 = establishmentService.readById(1L).orElse(null);
        Establishment res2 = establishmentService.readById(2L).orElse(null);
        Establishment res3 = establishmentService.readById(3L).orElse(null);

        Assertions.assertNotNull(res1);

        Assertions.assertIterableEquals(List.of(e1.getId(),
                                                e1.getName(),
                                                e1.getAddress(),
                                                e1.getType()),
                                        List.of(res1.getId(),
                                                res1.getName(),
                                                res1.getAddress(),
                                                res1.getType()));

        Assertions.assertNotNull(res2);

        Assertions.assertIterableEquals(List.of(e2.getId(),
                                                e2.getName(),
                                                e2.getAddress(),
                                                e2.getType()),
                                        List.of(res2.getId(),
                                                res2.getName(),
                                                res2.getAddress(),
                                                res2.getType()));

        Assertions.assertNotNull(res3);

        Assertions.assertIterableEquals(List.of(e3.getId(),
                                                e3.getName(),
                                                e3.getAddress(),
                                                e3.getType()),
                                        List.of(res3.getId(),
                                                res3.getName(),
                                                res3.getAddress(),
                                                res3.getType()));

        Mockito.verify(establishmentRepository).findById(e1.getId());
        Mockito.verify(establishmentRepository).findById(e2.getId());
        Mockito.verify(establishmentRepository).findById(e3.getId());
    }

    @Test
    public void testReadByIdNonExisting() {
        Mockito.when(establishmentRepository.findById(775L)).thenReturn(Optional.empty());
        Assertions.assertEquals(Optional.empty(), establishmentService.readById(775L));
    }

    @Test
    public void testReadAll() {
        Establishment e1 = new Establishment(1L, "dateTime1", "dateTime2", "test");
        Establishment e2 = new Establishment(2L, "dateTime3", "dateTime4", "test");
        Establishment e3 = new Establishment(3L, "dateTime5", "dateTime6", "test");
        List<Establishment> establishments = List.of(e1, e2, e3);

        Mockito.when(establishmentRepository.findAll()).thenReturn(establishments);

        List<Establishment> results = establishmentService.readAll().stream().toList();

        Assertions.assertIterableEquals(
                List.of(establishments.get(0).getId(),
                        establishments.get(0).getName(),
                        establishments.get(0).getAddress(),
                        establishments.get(0).getType()
                ),
                List.of(results.get(0).getId(),
                        results.get(0).getName(),
                        results.get(0).getAddress(),
                        results.get(0).getType()));

        Assertions.assertIterableEquals(
                List.of(establishments.get(1).getId(),
                        establishments.get(1).getName(),
                        establishments.get(1).getAddress(),
                        establishments.get(1).getType()
                ),
                List.of(results.get(1).getId(),
                        results.get(1).getName(),
                        results.get(1).getAddress(),
                        results.get(1).getType()));

        Assertions.assertIterableEquals(
                List.of(establishments.get(2).getId(),
                        establishments.get(2).getName(),
                        establishments.get(2).getAddress(),
                        establishments.get(2).getType()
                ),
                List.of(results.get(2).getId(),
                        results.get(2).getName(),
                        results.get(2).getAddress(),
                        results.get(2).getType()));

        Mockito.verify(establishmentRepository).findAll();
    }

    @Test
    public void testUpdateSuccess() {
        Establishment eToUpdate = new Establishment(1L, "dateTime2", "dateTime3", "test");
        Establishment eUpdated = new Establishment(1L, "dateTime2", "dateTime7", "test");
        Mockito.when(establishmentRepository.save(eUpdated)).thenReturn(eUpdated);
        Mockito.when(establishmentRepository.findById(1L)).thenReturn(Optional.of(eToUpdate));

        Establishment res = establishmentService.update(eUpdated);

        Assertions.assertIterableEquals(List.of(eUpdated.getId(),
                        eUpdated.getName(),
                        eUpdated.getAddress(),
                        eUpdated.getType()),
                List.of(res.getId(),
                        res.getName(),
                        res.getAddress(),
                        res.getType()));

    }

    @Test
    public void testUpdateNonExisting() {
        Establishment eUpdated = new Establishment(775L, "dateTime2", "dateTime7", "test");
        Mockito.when(establishmentRepository.findById(775L)).thenReturn(Optional.empty());
        Assertions.assertThrows(NonExistingEntityException.class, () -> establishmentService.update(eUpdated));
    }

    @Test
    public void testUpdateWithNullFields() {
        Establishment eToUpdate = new Establishment(1L, "dateTime2", "dateTime3", "test");
        Mockito.when(establishmentRepository.findById(1L)).thenReturn(Optional.of(eToUpdate));
        Establishment aUpdatedWithNull = new Establishment(1L, null, "dateTime1", "test");
        Assertions.assertThrows(NullEntityFieldException.class, () -> establishmentService.create(aUpdatedWithNull));
    }
    @Test
    public void testDeleteByIdSuccess() {
        Establishment eToDelete = new Establishment(1L, "dateTime2", "dateTime3", "test");
        Mockito.when(establishmentRepository.existsById(1L)).thenReturn(Boolean.TRUE);
        Mockito.when(establishmentRepository.findById(1L)).thenReturn(Optional.of(eToDelete));
        establishmentService.deleteById(1L);
        Mockito.verify(establishmentRepository).deleteById(1L);
    }

    @Test
    public void testDeleteByIdNonExisting() {
        Mockito.when(establishmentRepository.existsById(775L)).thenReturn(Boolean.FALSE);
        Assertions.assertThrows(DeleteNonExistingEntityException.class, () -> establishmentService.deleteById(775L));
    }
}