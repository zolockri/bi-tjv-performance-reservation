package cz.cvut.fit.tjv.performance_reservation.api.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import cz.cvut.fit.tjv.performance_reservation.api.converter.ArtistConverter;
import cz.cvut.fit.tjv.performance_reservation.api.dto.ArtistDto;
import cz.cvut.fit.tjv.performance_reservation.business.ArtistService;
import cz.cvut.fit.tjv.performance_reservation.business.exceptions.DeleteNonExistingEntityException;
import cz.cvut.fit.tjv.performance_reservation.business.exceptions.NonExistingEntityException;
import cz.cvut.fit.tjv.performance_reservation.business.exceptions.NullEntityFieldException;
import cz.cvut.fit.tjv.performance_reservation.domain.Artist;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;


import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;
import java.util.Optional;

import static cz.cvut.fit.tjv.performance_reservation.api.converter.ArtistConverter.toDto;
import static org.mockito.Mockito.doThrow;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

@WebMvcTest(ArtistController.class)
public class ArtistControllerTest {
    @MockBean
    ArtistService artistService;
    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    MockMvc mockMvc;

    @Test
    public void testCreateSuccess() throws Exception {
        Artist aCreated = new Artist(null, "test", "test");
        Artist aCreatedWithId = new Artist(1L, "test", "test");
        Mockito.when(artistService.create(aCreated)).thenReturn(aCreatedWithId);

        mockMvc.perform(post("/artists")
                        .contentType(APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(toDto(aCreated))))
                .andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(toDto(aCreatedWithId))));
    }

    @Test
    public void testCreateWithNullFields() throws Exception {
        Artist aWithNullFields = new Artist(null, null, "arserock");
        Mockito.when(artistService.create(aWithNullFields)).thenThrow(NullEntityFieldException.class);
        mockMvc.perform(post("/artists")
                        .contentType(APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(toDto(aWithNullFields))))
                .andExpect(status().isUnprocessableEntity())
                .andExpect(content().string("Entity null field"));
    }

    @Test
    public void testReadSingleEntity() throws Exception {
        Artist a1 = new Artist(1L, "test1", "test1");
        Artist a2 = new Artist(2L, "test2", "test2");
        Artist a3 = new Artist(3L, "test3", "test3");

        Mockito.when(artistService.readById(1L)).thenReturn(Optional.of(a1));
        Mockito.when(artistService.readById(2L)).thenReturn(Optional.of(a2));
        Mockito.when(artistService.readById(3L)).thenReturn(Optional.of(a3));

        mockMvc.perform(get("/artists/1"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_JSON))
                .andExpect(content().json(objectMapper.writeValueAsString(toDto(a1))));
        mockMvc.perform(get("/artists/2"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_JSON))
                .andExpect(content().json(objectMapper.writeValueAsString(toDto(a2))));
        mockMvc.perform(get("/artists/3"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_JSON))
                .andExpect(content().json(objectMapper.writeValueAsString(toDto(a3))));
    }

    @Test
    public void testReadNonExistingEntity() throws Exception {
        Mockito.when(artistService.readById(775L)).thenReturn(Optional.empty());
        mockMvc.perform(get("/artists/775"))
                .andExpect(status().isNotFound())
                .andExpect(content().string("Entity not found"));
    }

    @Test
    public void testReadAll() throws Exception {
        Artist a1 = new Artist(1L, "estestest", "test1");
        Artist a2 = new Artist(2L, "estestest", "test2");
        Artist a3 = new Artist(3L, "estestest", "test3");

        List<Artist> artists = List.of(a1, a2, a3);

        Mockito.when(artistService.readAll()).thenReturn(artists);

        List<ArtistDto> artistsDto = artists.stream().map(ArtistConverter::toDto).toList();
        mockMvc.perform(get("/artists"))
                .andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(artistsDto)));
    }

    @Test
    public void testUpdateSuccess() throws Exception {
        Artist artistUpdated = new Artist(1L, "lol", null);
        Mockito.when(artistService.update(artistUpdated)).thenReturn(artistUpdated);
        mockMvc.perform(put("/artists/1")
                        .contentType(APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(toDto(artistUpdated))))
                .andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_JSON))
                .andExpect(content().json(objectMapper.writeValueAsString(toDto(artistUpdated))));
    }

    @Test
    public void testUpdateWithNullFields() throws Exception {
        Artist artistUpdatedWithNullFields = new Artist(1L, null, null);
        Mockito.when(artistService.update(artistUpdatedWithNullFields)).thenThrow(NullEntityFieldException.class);
        mockMvc.perform(put("/artists/1")
                        .contentType(APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(toDto(artistUpdatedWithNullFields))))
                .andExpect(status().isUnprocessableEntity())
                .andExpect(content().string("Entity null field"));
    }

    @Test
    public void testUpdateNonExistingEntity() throws Exception {
        Artist artistUpdateNonExisting = new Artist(12345L, "blabla", null);
        Mockito.when(artistService.update(artistUpdateNonExisting)).thenThrow(NonExistingEntityException.class);
        mockMvc.perform(put("/artists/12345")
                        .contentType(APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(toDto(artistUpdateNonExisting))))
                .andExpect(status().isNotFound())
                .andExpect(content().string("Entity not found"));
    }

    @Test
    public void testDeleteExisting() throws Exception {
        mockMvc.perform(delete("/artists/1"))
                .andExpect(status().isNoContent())
                .andExpect(content().string(""));
    }

    @Test
    public void testDeleteNonExisting() throws Exception {
        doThrow(DeleteNonExistingEntityException.class).when(artistService).deleteById(12345L);
        mockMvc.perform(delete("/artists/12345"))
                .andExpect(status().isNoContent())
                .andExpect(content().string(""));
    }
}