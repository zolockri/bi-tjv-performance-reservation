package cz.cvut.fit.tjv.performance_reservation.business;

import cz.cvut.fit.tjv.performance_reservation.business.exceptions.DeleteNonExistingEntityException;
import cz.cvut.fit.tjv.performance_reservation.business.exceptions.NonExistingEntityException;
import cz.cvut.fit.tjv.performance_reservation.business.exceptions.NullEntityFieldException;
import cz.cvut.fit.tjv.performance_reservation.dao.ArtistJpaRepository;
import cz.cvut.fit.tjv.performance_reservation.domain.Artist;
import cz.cvut.fit.tjv.performance_reservation.domain.Performance;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.time.LocalDateTime;
import java.util.*;

import static org.mockito.ArgumentMatchers.any;

@SpringBootTest
public class ArtistServiceTest {
    @Autowired
    private ArtistService artistService;

    @MockBean
    private ArtistJpaRepository artistRepository;

    @BeforeEach
    public void setUp() {

    }

    @Test
    public void testCreateSuccess() {
        Artist aCreated = new Artist(null, "test", "test");
        Artist aCreatedWithId = new Artist(1L, "test", "test");

        Mockito.when(artistRepository.save(aCreated)).thenReturn(aCreatedWithId);

        Artist res = artistService.create(aCreated);

        Assertions.assertIterableEquals(List.of(aCreatedWithId.getId(),
                                                aCreatedWithId.getName(),
                                                aCreatedWithId.getGenre()),
                                        List.of(res.getId(),
                                                res.getName(),
                                                res.getGenre()));

        Mockito.verify(artistRepository).save(aCreated);
    }

    @Test
    public void testCreateWithNullFields() {
        Artist aCreatedWithNull = new Artist(1L, null, "test");
        Assertions.assertThrows(NullEntityFieldException.class, () -> artistService.create(aCreatedWithNull));
    }

    @Test
    public void testReadByIdSuccess() {
        Artist a1 = new Artist(1L, "dateTime1", "dateTime2");
        Artist a2 = new Artist(2L, "dateTime3", "dateTime4");
        Artist a3 = new Artist(3L, "dateTime5", "dateTime6");

        Mockito.when(artistRepository.findById(1L)).thenReturn(Optional.of(a1));
        Mockito.when(artistRepository.findById(2L)).thenReturn(Optional.of(a2));
        Mockito.when(artistRepository.findById(3L)).thenReturn(Optional.of(a3));

        Artist res1 = artistService.readById(1L).orElse(null);
        Artist res2 = artistService.readById(2L).orElse(null);
        Artist res3 = artistService.readById(3L).orElse(null);

        Assertions.assertNotNull(res1);
        Assertions.assertNotNull(res2);
        Assertions.assertNotNull(res3);

        Assertions.assertIterableEquals(List.of(a1.getId(),
                                                a1.getName(),
                                                a1.getGenre()),
                                        List.of(res1.getId(),
                                                res1.getName(),
                                                res1.getGenre()));

        Assertions.assertIterableEquals(List.of(a2.getId(),
                                                a2.getName(),
                                                a2.getGenre()),
                                        List.of(res2.getId(),
                                                res2.getName(),
                                                res2.getGenre()));

        Assertions.assertIterableEquals(List.of(a3.getId(),
                                                a3.getName(),
                                                a3.getGenre()),
                                        List.of(res3.getId(),
                                                res3.getName(),
                                                res3.getGenre()));

        Mockito.verify(artistRepository).findById(a1.getId());
        Mockito.verify(artistRepository).findById(a2.getId());
        Mockito.verify(artistRepository).findById(a3.getId());
    }

    @Test
    public void testReadByIdNonExisting() {
        Mockito.when(artistRepository.findById(775L)).thenReturn(Optional.empty());
        Assertions.assertEquals(Optional.empty(), artistService.readById(775L));
    }

    @Test
    public void testReadAll() {
        Artist a1 = new Artist(1L, "dateTime1", "dateTime2");
        Artist a2 = new Artist(2L, "dateTime3", "dateTime4");
        Artist a3 = new Artist(3L, "dateTime5", "dateTime6");
        List<Artist> artists = List.of(a1, a2, a3);

        Mockito.when(artistRepository.findAll()).thenReturn(artists);

        List<Artist> results = artistService.readAll().stream().toList();

        Assertions.assertIterableEquals(
                List.of(artists.get(0).getId(),
                        artists.get(0).getName(),
                        artists.get(0).getGenre()
                ),
                List.of(results.get(0).getId(),
                        results.get(0).getName(),
                        results.get(0).getGenre()));

        Assertions.assertIterableEquals(
                List.of(artists.get(1).getId(),
                        artists.get(1).getName(),
                        artists.get(1).getGenre()
                ),
                List.of(results.get(1).getId(),
                        results.get(1).getName(),
                        results.get(1).getGenre()));

        Assertions.assertIterableEquals(
                List.of(artists.get(2).getId(),
                        artists.get(2).getName(),
                        artists.get(2).getGenre()
                ),
                List.of(results.get(2).getId(),
                        results.get(2).getName(),
                        results.get(2).getGenre()));

        Mockito.verify(artistRepository).findAll();
    }

    @Test
    public void testUpdateSuccess() {
        Artist aToUpdate = new Artist(1L, "dateTime2", "dateTime3");
        Artist aUpdated = new Artist(1L, "dateTime2", "dateTime7");
        Mockito.when(artistRepository.save(aUpdated)).thenReturn(aUpdated);
        Mockito.when(artistRepository.findById(1L)).thenReturn(Optional.of(aToUpdate));

        Artist res = artistService.create(aUpdated);

        Assertions.assertIterableEquals(List.of(aUpdated.getId(),
                        aUpdated.getName(),
                        aUpdated.getGenre()),
                List.of(res.getId(),
                        res.getName(),
                        res.getGenre()));

    }

    @Test
    public void testUpdateNonExisting() {
        Artist aUpdated = new Artist(775L, "dateTime2", "dateTime7");
        Mockito.when(artistRepository.findById(775L)).thenReturn(Optional.empty());
        Assertions.assertThrows(NonExistingEntityException.class, () -> artistService.update(aUpdated));
    }

    @Test
    public void testUpdateWithNullFields() {
        Artist aToUpdate = new Artist(1L, "dateTime2", "dateTime3");
        Mockito.when(artistRepository.findById(1L)).thenReturn(Optional.of(aToUpdate));
        Artist aUpdatedWithNull = new Artist(1L, null, "dateTime1");
        Assertions.assertThrows(NullEntityFieldException.class, () -> artistService.create(aUpdatedWithNull));
    }

    @Test
    public void testDeleteByIdSuccess() {
        Artist aToDelete = new Artist(1L, "dateTime2", "dateTime3");
        Mockito.when(artistRepository.existsById(1L)).thenReturn(Boolean.TRUE);
        Mockito.when(artistRepository.findById(1L)).thenReturn(Optional.of(aToDelete));
        artistService.deleteById(1L);
        Mockito.verify(artistRepository).deleteById(1L);
    }

    @Test
    public void testDeleteByIdNonExisting() {
        Mockito.when(artistRepository.existsById(775L)).thenReturn(Boolean.FALSE);
        Assertions.assertThrows(DeleteNonExistingEntityException.class, () -> artistService.deleteById(775L));
    }

    @Test
    public void testDeleteAllArtistsNotPerformedForAYear() {
        LocalDateTime oxxxyTime1 = LocalDateTime.of(2022, 3, 17, 20, 0);
        LocalDateTime oxxxyTime2 = LocalDateTime.of(2022, 3, 18, 0, 0);

        LocalDateTime oxxxyTime3 = LocalDateTime.of(2023, 3, 24, 20, 0);
        LocalDateTime oxxxyTime4 = LocalDateTime.of(2023, 3, 25, 0, 0);

        LocalDateTime slavaTime1 = LocalDateTime.of(2022, 10, 15, 20, 0);
        LocalDateTime slavaTime2 = LocalDateTime.of(2022, 10, 15, 20, 0, 0, 1);

        LocalDateTime huskyTime1 = LocalDateTime.of(2020, 10, 15, 20, 0);
        LocalDateTime huskyTime2 = LocalDateTime.of(2020, 10, 15, 23, 0);


        Artist oxxxymiron = new Artist(1L, "Oxxxymiron", "hip hop");
        Artist slava = new Artist(2L, "Slava KPSS", "battle rap");
        Artist husky = new Artist(3L, "Husky", "rap");

        Performance oxxxyP1 = new Performance(4L, oxxxyTime1, oxxxyTime2);
        Performance oxxxyP2 = new Performance(5L, oxxxyTime3, oxxxyTime4);
        Performance slavaP1 = new Performance(6L, slavaTime1, slavaTime2);
        Performance huskyP1 = new Performance(7L, huskyTime1, huskyTime2);

        oxxxyP1.setPerformingArtists(new HashSet<>(Set.of(oxxxymiron)));
        oxxxyP2.setPerformingArtists(new HashSet<>(Set.of(oxxxymiron)));
        slavaP1.setPerformingArtists(new HashSet<>(Set.of(slava)));
        huskyP1.setPerformingArtists(new HashSet<>(Set.of(husky)));

        oxxxymiron.setArtistsPerformances(new HashSet<>(Set.of(oxxxyP1, oxxxyP2)));
        slava.setArtistsPerformances(new HashSet<>(Set.of(slavaP1)));
        husky.setArtistsPerformances(new HashSet<>(Set.of(huskyP1)));

        Mockito.when(artistRepository.findAllArtistsPerformedAfterDate(any(LocalDateTime.class)))
                .thenReturn(List.of(oxxxymiron, slava));

        Mockito.when(artistRepository.findAll()).thenReturn(
                new ArrayList<>(List.of(oxxxymiron, slava, husky)));

        Mockito.when(artistRepository.existsById(1L)).thenReturn(Boolean.TRUE);
        Mockito.when(artistRepository.existsById(2L)).thenReturn(Boolean.TRUE);
        Mockito.when(artistRepository.existsById(3L)).thenReturn(Boolean.TRUE);

        Mockito.when(artistRepository.findById(1L)).thenReturn(Optional.of(oxxxymiron));
        Mockito.when(artistRepository.findById(2L)).thenReturn(Optional.of(slava));
        Mockito.when(artistRepository.findById(3L)).thenReturn(Optional.of(husky));

        artistService.deleteAllArtistsNotPerformedForAYear();

        Mockito.verify(artistRepository).deleteById(3L);
        Mockito.verify(artistRepository, Mockito.never()).deleteById(1L);
        Mockito.verify(artistRepository, Mockito.never()).deleteById(2L);
    }
}