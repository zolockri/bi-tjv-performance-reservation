# BI-TJV  Performance reservation

### Description

The application is supposed to be a simple performance reservation system for music Artists in cooperation with entertainment Establishments. It has 3 entities: _Establishment_, _Performance_, and _Artist_. _Establishment_, in turn, provides its place for _Artists_ to perform in order to attract more guests. _Artists_ would be able to sign in to a _Performance_ to perform and, as a main interest, get recognizible and grow their career.

##### Integrity Constrains
1. An Artist can't sign up for another Performance, if they have already signed up to another one at the same time
2. datetime_from < datetime_to

##### Non-CRUD Operation
For now it would have one non-CRUD operation which is 'remove Artist that haven't been performing for one year'.

---

### Run application
##### Docker (DB) 
1. docker run -d --name tjv_postgres -p 5432:5432 -e POSTGRES_USER=username -e POSTGRES_PASSWORD=password -v /local/path:/postgres postgres

---

### Generate documentation
1. Run server
2. Visit http://localhost:8080/v3/api-docs.yaml