openapi: 3.0.1
info:
  title: OpenAPI definition
  version: v0
servers:
- url: http://localhost:8080
  description: Generated server url
paths:
  /performances/{perfId}/artists/{artId}:
    put:
      tags:
      - performance-controller
      summary: Update artist of performance
      operationId: updateArtistOfPerformance
      parameters:
      - name: perfId
        in: path
        required: true
        schema:
          type: integer
          format: int64
      - name: artId
        in: path
        required: true
        schema:
          type: integer
          format: int64
      responses:
        "204":
          description: No Content
        "404":
          description: Not Found
          content:
            text/plain: {}
        "409":
          description: Conflict
          content:
            text/plain: {}
    delete:
      tags:
      - performance-controller
      summary: Delete artist of performance
      operationId: deleteArtistOfPerformance
      parameters:
      - name: perfId
        in: path
        required: true
        schema:
          type: integer
          format: int64
      - name: artId
        in: path
        required: true
        schema:
          type: integer
          format: int64
      responses:
        "204":
          description: No Content
        "404":
          description: Not Found
          content:
            text/plain: {}
  /performances/{id}:
    get:
      tags:
      - performance-controller
      summary: Read performance by ID
      operationId: readById
      parameters:
      - name: id
        in: path
        required: true
        schema:
          type: integer
          format: int64
      responses:
        "200":
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/PerformanceDto_ViewWithId'
        "404":
          description: Performance not found
          content:
            text/plain: {}
    put:
      tags:
      - performance-controller
      summary: Update performance
      operationId: update
      parameters:
      - name: id
        in: path
        required: true
        schema:
          type: integer
          format: int64
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/PerformanceDto_ViewWithoutId'
        required: true
      responses:
        "200":
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/PerformanceDto_ViewWithId'
        "404":
          description: Not Found
          content:
            text/plain: {}
        "422":
          description: Unprocessable Entity
          content:
            text/plain: {}
        "409":
          description: Conflict
          content:
            text/plain: {}
    delete:
      tags:
      - performance-controller
      summary: Delete performance by ID
      operationId: deleteById
      parameters:
      - name: id
        in: path
        required: true
        schema:
          type: integer
          format: int64
      responses:
        "204":
          description: No Content
  /establishments/{id}:
    get:
      tags:
      - establishment-controller
      summary: Read establishment by ID
      operationId: readById_1
      parameters:
      - name: id
        in: path
        required: true
        schema:
          type: integer
          format: int64
      responses:
        "200":
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/EstablishmentDto_ViewWithId'
        "404":
          description: Establishment not found
          content:
            text/plain: {}
    put:
      tags:
      - establishment-controller
      summary: Update establishment
      operationId: update_1
      parameters:
      - name: id
        in: path
        required: true
        schema:
          type: integer
          format: int64
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/EstablishmentDto_ViewWithoutId'
        required: true
      responses:
        "404":
          description: Not Found
          content:
            text/plain: {}
        "200":
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/EstablishmentDto_ViewWithId'
        "422":
          description: Unprocessable Entity
          content:
            text/plain: {}
    delete:
      tags:
      - establishment-controller
      summary: Delete establishment by ID
      operationId: deleteById_1
      parameters:
      - name: id
        in: path
        required: true
        schema:
          type: integer
          format: int64
      responses:
        "204":
          description: No Content
  /establishments/{establishmentId}/performances/{performanceId}:
    put:
      tags:
      - establishment-controller
      summary: Update performance to establishment
      operationId: updatePerformanceToEstablishment
      parameters:
      - name: establishmentId
        in: path
        required: true
        schema:
          type: integer
          format: int64
      - name: performanceId
        in: path
        required: true
        schema:
          type: integer
          format: int64
      responses:
        "204":
          description: No Content
        "404":
          description: Not Found
          content:
            text/plain: {}
  /artists/{id}:
    get:
      tags:
      - artist-controller
      summary: Read artist by ID
      operationId: readById_2
      parameters:
      - name: id
        in: path
        required: true
        schema:
          type: integer
          format: int64
      responses:
        "200":
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/PerformanceDto_ViewWithId'
        "404":
          description: Artist not found
          content:
            text/plain: {}
    put:
      tags:
      - artist-controller
      summary: Update artist
      operationId: update_2
      parameters:
      - name: id
        in: path
        required: true
        schema:
          type: integer
          format: int64
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/ArtistDto_ViewWithoutId'
        required: true
      responses:
        "404":
          description: Not Found
          content:
            text/plain: {}
        "422":
          description: Unprocessable Entity
          content:
            text/plain: {}
        "200":
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ArtistDto_ViewWithId'
    delete:
      tags:
      - artist-controller
      summary: Delete artist by ID
      operationId: deleteById_2
      parameters:
      - name: id
        in: path
        required: true
        schema:
          type: integer
          format: int64
      responses:
        "204":
          description: No Content
  /performances:
    get:
      tags:
      - performance-controller
      summary: Read all performances
      operationId: readAll
      responses:
        "200":
          description: OK
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/PerformanceDto_ViewWithId'
    post:
      tags:
      - performance-controller
      summary: Create performance
      operationId: create
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/PerformanceDto_ViewWithoutId'
        required: true
      responses:
        "200":
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/PerformanceDto_ViewWithId'
  /establishments:
    get:
      tags:
      - establishment-controller
      summary: Read all establishments
      operationId: readAll_1
      responses:
        "200":
          description: OK
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/EstablishmentDto_ViewWithId'
    post:
      tags:
      - establishment-controller
      summary: Create establishment
      operationId: create_1
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/EstablishmentDto_ViewWithoutId'
        required: true
      responses:
        "200":
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/EstablishmentDto_ViewWithId'
  /artists:
    get:
      tags:
      - artist-controller
      summary: Read all artist
      operationId: readAll_2
      responses:
        "200":
          description: OK
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/ArtistDto_ViewWithId'
    post:
      tags:
      - artist-controller
      summary: Create artist
      operationId: create_2
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/ArtistDto_ViewWithoutId'
        required: true
      responses:
        "200":
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ArtistDto_ViewWithId'
  /performances/{perfId}/artists:
    get:
      tags:
      - performance-controller
      summary: Read artists of performance
      operationId: readArtistsOfPerformance
      parameters:
      - name: perfId
        in: path
        required: true
        schema:
          type: integer
          format: int64
      responses:
        "404":
          description: Not Found
          content:
            text/plain: {}
        "200":
          description: OK
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/ArtistDto_ViewWithId'
  /performances/{id}/establishments:
    get:
      tags:
      - performance-controller
      summary: Read establishments to performance
      operationId: readEstablishmentToPerformance
      parameters:
      - name: id
        in: path
        required: true
        schema:
          type: integer
          format: int64
      responses:
        "200":
          description: OK
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/EstablishmentDto_ViewWithId'
        "404":
          description: Not Found
          content:
            text/plain: {}
    delete:
      tags:
      - performance-controller
      summary: Delete establishment to performance
      operationId: deleteEstablishmentToPerformance
      parameters:
      - name: id
        in: path
        required: true
        schema:
          type: integer
          format: int64
      responses:
        "204":
          description: No Content
        "404":
          description: Not Found
          content:
            text/plain: {}
  /establishments/{estId}/performances:
    get:
      tags:
      - establishment-controller
      summary: Read performances in establishment
      operationId: readPerformancesInEstablishment
      parameters:
      - name: estId
        in: path
        required: true
        schema:
          type: integer
          format: int64
      responses:
        "404":
          description: Not Found
          content:
            text/plain: {}
        "200":
          description: OK
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/PerformanceDto_ViewWithId'
  /artists/{artId}/performances:
    get:
      tags:
      - artist-controller
      summary: Read performances of artist
      operationId: readPerformancesOfArtist
      parameters:
      - name: artId
        in: path
        required: true
        schema:
          type: integer
          format: int64
      responses:
        "404":
          description: Not Found
          content:
            text/plain: {}
        "200":
          description: OK
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/PerformanceDto_ViewWithId'
  /old_artists:
    delete:
      tags:
      - artist-controller
      summary: Delete all artists who haven't performed for year
      operationId: deleteAllArtistsNotPerformedForAYear
      responses:
        "204":
          description: No Content
components:
  schemas:
    PerformanceDto_ViewWithoutId:
      required:
      - dateTimeFrom
      - dateTimeTo
      type: object
      properties:
        dateTimeFrom:
          type: string
          example: 24/05/2015 19:30
        dateTimeTo:
          type: string
          example: 24/05/2015 23:30
    PerformanceDto_ViewWithId:
      required:
      - dateTimeFrom
      - dateTimeTo
      - id
      type: object
      properties:
        id:
          type: integer
          format: int64
        dateTimeFrom:
          type: string
          example: 24/05/2015 19:30
        dateTimeTo:
          type: string
          example: 24/05/2015 23:30
    EstablishmentDto_ViewWithoutId:
      required:
      - name
      type: object
      properties:
        name:
          type: string
          example: Forum Karlín
        address:
          type: string
          example: "Pernerova 51, 186 00 Praha 8-Karlín"
        type:
          type: string
          example: Event venue
    EstablishmentDto_ViewWithId:
      required:
      - id
      - name
      type: object
      properties:
        id:
          type: integer
          format: int64
        name:
          type: string
          example: Forum Karlín
        address:
          type: string
          example: "Pernerova 51, 186 00 Praha 8-Karlín"
        type:
          type: string
          example: Event venue
    ArtistDto_ViewWithoutId:
      required:
      - name
      type: object
      properties:
        name:
          type: string
          example: Beastie Boys
        genre:
          type: string
          example: hip hop
    ArtistDto_ViewWithId:
      required:
      - id
      - name
      type: object
      properties:
        id:
          type: integer
          format: int64
        name:
          type: string
          example: Beastie Boys
        genre:
          type: string
          example: hip hop
